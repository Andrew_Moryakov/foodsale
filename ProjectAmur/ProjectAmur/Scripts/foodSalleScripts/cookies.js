﻿
function getCookie(name) {
    return $.cookie(name);
}

function removeCokie(name) {
    // $.cookie(name, null); 
    $.removeCookie(name, { path: '/User/Index' });
}

function setCookie(name, value, day, path) {
    $.cookie(name, value, { expires: day, path: "/" + path });
}