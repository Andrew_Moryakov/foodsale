﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using ProjectAmur.Models;

namespace ProjectAmur.Models
{
    public class OrderCollection
    {
        public Boolean Discount { get; set; }

        public Dictionary<String, NumberFood> Numbers { get; set; }

        public Dictionary<String, int> Table { get; set; }

        public OrderCollection()
        {
        }

    }

    public class NumberFood:Food
    {
        public NumberFood(Food food)
        {
            Category = food.Category;
            Title = food.Title;
            ImagePath = food.ImagePath;
            Description = food.Description;
            CategoryId = food.CategoryId;
            Id = food.Id;
            PriceWidths = food.PriceWidths;
            //Numbers = new List<NumberFood>();
            /* для совместимостис  C# 5
            Number = 0;
            */

        }

        public NumberFood()
        {

        }
        //public int Id { get; set; }
        //public Category Category { get; set; }

        //public int? CategoryId { get; set; }

        //public ICollection<PriceWidth> PriceWidths { get; set; }

        //public string Description { get; set; }

        //public string ImagePath { get; set; }

        //public string Title { get; set; }
        [Display(Name = "Количество")]
        [RegularExpression(@"/\d+/", ErrorMessage = "Цена не может быть отрицательной")]
        [NotMapped]
        public int Number { get; set; } = 0; //Количество копий этого блюда в заказе

        [Display(Name = "Цена блюда")]
        [Required(ErrorMessage = "Выберите цену")]
        [NotMapped]
        public int? SelectedPriceWidths { get; set; } = -1;

    }
}
