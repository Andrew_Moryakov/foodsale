﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Data.SqlServerCe;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjectAmur.Models.Context;

namespace ProjectAmur.Models
{
    public class ApplicationUser : IdentityUser
    {
        [DisplayName("Имя")]
        public String Name { get; set; } // добавляем свойство Имя
        public String SurName { get; set; } // добавляем свойство Имя
        public Boolean Actual { get; set; }
        public String RandomKey { get; set; }//при открытии сессии, генерируем этот ключ, добавляем к паролю пользователю и отправляем полученный хэш. При закрытии сессии, этот ключ стирается.

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync( UserManager<ApplicationUser> manager )
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync( this, DefaultAuthenticationTypes.ApplicationCookie );
            // Add custom user claims here
            return userIdentity;
        }
    }
    #region Custom classes
    public class Category
    {
        public int Id { get; set; }

        [Display( Name = "Название категории" )]
        [Required( ErrorMessage = "У категории должно быть имя" )]
        public String Title { get; set; }

        [Display( Name = "Изображение" )]
        public String ImagePath { get; set; }
        [NotMapped]
        public HttpPostedFileBase[] ImageFile { get; set; }

        [Display( Name = "Описание" )]
        public String Description { get; set; }

        [Display(Name = "Возможна ли скидка на продукт ?")]
        public Boolean DiscountBool { get; set; }

        [Display(Name = "Размер скидки")]
        public Double Discount { get; set; }

        [Display( Name = "Родительская категория" )]
        public int? CategoryId { get; set; }
        [ForeignKey( "CategoryId" )]
        public Category ParentCategory { get; set; }
        public Boolean Actual { get; set; }
        [NotMapped]
        public ICollection<Category> SubCategories { get; set; }

        public Boolean Visible { get; set; }
        public Category()
        {
            SubCategories = new List<Category>();
        }
    }

    public class Department
    {
        public int Id { get; set; }
        [Display(Name = "название отдела")]
        public String Title { get; set; }
        [Display(Name = "Описание")]
        public String Discription { get; set; }
        public Boolean Actual { get; set; }
        public Boolean Visible { get; set; }
    }

    public class Food
    {
        public int Id { get; set; }

        [Display( Name = "Блюдо" )]
        [Required( ErrorMessage = "У блюда должно быть имя" )]
        public String Title { get; set; }

        [Display(Name = "Изображение")]
        public String ImagePath { get; set; }
        [NotMapped]
        [Display(Name = "Изображение")]
        public HttpPostedFileBase[] ImageFile { get; set; }

        [Display( Name = "Категория" )]
        public int? CategoryId { get; set; }
        [ForeignKey( "CategoryId" )]
        public Category Category { get; set; }

        [Display(Name = "Категория")]
        public int? DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        [Display( Name = "Описание" )]
        public String Description { get; set; }

        //public int? PriceWidthId { get; set; }
        //[ForeignKey("PriceWidthId")]
        //public PriceWidth PriceWidth { get; set; }
        [NotMapped]
        public Dictionary<String, PriceWidth> PriceWidthsKey { get; set; }
        //[NotMapped]
        //public String SelectedPrice { get; set; }
        [NotMapped]
        public int Number { get; set; }
        ////[ForeignKey("PriceWidth")]
        public Boolean Actual { get; set; }
        public ICollection<PriceWidth> PriceWidths { get; set; }//Так как при создании блюда можно выбрать сколько угодно цен, то нам нужен саписок цен
        public Food( )
        {
            PriceWidthsKey = new Dictionary<string, PriceWidth>();
            PriceWidths = new List<PriceWidth>();
        }

        public Boolean Visible { get; set; }

        public Food(Food food)
        {
            Title = food.Title;
            Description = food.Description;
            ImagePath = food.ImagePath;
            ImageFile = food.ImageFile;

            Category = food.Category;
            PriceWidths = food.PriceWidths;
        }



    }

    public class CurrencyName
    {
        public int Id { get; set; }
        [Display(Name = "Название валюты")]
        [Required(ErrorMessage = "Введите название валюты")]
        public String Name { get; set; }
        [Display(Name = "Краткое название валюты. Например руб.")]
        [Required(ErrorMessage = "Введите краткое название валюты")]
        public String Prefix { get; set; }
    }
    public class WeightName
    {
        public int Id { get; set; }
        [Display(Name = "Название веса")]
        [Required(ErrorMessage = "Введите название веса")]
        public String Name { get; set; }
        [Display(Name = "Краткое название веса. Например гр.")]
        [Required(ErrorMessage = "Введите краткое название веса")]
        public String Prefix { get; set; }
    }
    public class WorkingDay
    {
        public int Id { get; set; }
        public DateTime? ClosingDay {get; set; }
        public DateTime OpeningDay { get; set; }
      //  public double[] Income { get; set; }

        public String UserId { get; set; }
        [ForeignKey("UserId")]
        [DisplayName("Официант")]
        public ApplicationUser Employee { get; set; }
    }


    
    //http://www.mortenanderson.net/code-first-migrations-for-entity-framework
    public class PriceWidth
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Вес блюда")]
        public String Width { get; set; }

        //[Required(ErrorMessage = "Выбирети вес")]
        public int? WeightNameId { get; set; }
        [Display(Name = "Весовка")]
        [ForeignKey("WeightNameId")]
        public WeightName WeightName { get; set; }

        //[Required(ErrorMessage = "Выбирети валюту")]
        public int? CurrencyNameId { get; set; }
        [Display(Name = "Валюта")]
        [ForeignKey("CurrencyNameId")]
        public CurrencyName CurrencyName { get; set; }

        [Display(Name = "Цена блюда")]
        //[RegularExpression( @"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$", ErrorMessage = "eMail is not in proper format" )]
        [Required(ErrorMessage = "Добавьте цену")]
        public Double Price { get; set; }

        public DateTime DataChange { get; set; }
        public Boolean Actual { get; set; }

        [NotMapped]
        public ICollection<Food> Foods { get; set; }

        //[NotMapped]
        //public ICollection<CurrencyName> CurrencyNames { get; set; }
        //[NotMapped]
        //public ICollection<WeightName> WeightNames { get; set; }
        public PriceWidth()
        {
            //CurrencyNames = new List<CurrencyName>();
            Foods = new List<Food>();
            //WeightNames = new List<WeightName>();
        }
    }

    public class Company
    {
        [Key]
        public int Id { get; set; }
        public String CompanyName { get; set; }
        public String PasswordCompany { get; set; }
    }

    public class Invoice
    {
        [Key]
        [DisplayName("Номер чека")]
        public int IdInvoice { get; set; }

        public int? FoodId { get; set; }
        [ForeignKey( "FoodId" )]
        public Food Food { get; set; }

        public int? DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        public int? PriceWidthId { get; set; }
        [ForeignKey("PriceWidthId")]
        public PriceWidth PriceWidth { get; set; }

        public int? TableId { get; set; }
        [ForeignKey( "TableId" )]
        public Table Table { get; set; }

        public int WorkingDayId { get; set; }
        [ForeignKey("WorkingDayId")]
        [DisplayName("Смена")]
        public WorkingDay WorkingDay { get; set; }

        public DateTime Date { get; set; }

        [DisplayName("Количество продаж в одном чеке")]
        public String Number { get; set; }

        public Double Price { get; set; } = -1;

        public String FoodTitle { get; set; }

        [DisplayName("Скидка")]
        public bool Discount { get; set; }

        public String UserId { get; set; }
        [ForeignKey("UserId")]
        [DisplayName("Официант")]
        public ApplicationUser Employee { get; set; }

        public Boolean Crypt { get; set; } = false;
    }

    public class Log
    {
        public int Id { get; set; }
        public String Title { get; set; }
        public int Discription { get; set; }

        public int UserId { get; set; }
        [NotMapped]
        [ForeignKey( "UserId" )]
        public ApplicationUser User { get; set; }

    }

    public class Comment
    {
        public int Id { get; set; }

        public String CommentText { get; set; }

        public int FoodId { get; set; }
        [ForeignKey( "FoodId" )]
        public Food Food { get; set; }

        public int TableId { get; set; }
        [ForeignKey( "TableId" )]
        public Table Table { get; set; }
    }


    public class Table
    {
        [Key]
        public int Id { get; set; }

        [Display( Name = "Заголовок стола" )]
        public String Title { get; set; }
        [Display( Name = "Описание" )]
        public String Discription { get; set; }
        public Boolean Actual { get; set; }

        public Boolean Visible { get; set; }
    }
    #endregion

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    { /*<add name = "DefaultConnection" connectionString="Data Source=|DataDirectory|Database1.sdf" password = "72453722" ;providerName="System.Data.SqlServerCe.4.0" />*/
        public ApplicationDbContext( )
            : base(new SqlCeConnection("Data Source=|DataDirectory|Database1.sdf; password = '72453722';"),
             contextOwnsConnection: true )
        {
        }

        public static ApplicationDbContext Create( )
        {
            return new ApplicationDbContext();
        }



        public DbSet<Food> Foods { get; set; }
        public DbSet<Category> Categories { get; set; }
        //   public DbSet<ApplicationUser> Users { get; set; }//?
        public DbSet<Log> Loges { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<Invoice> Invoises { get; set; }
        public DbSet<PriceWidth> PriceWidths { get; set; }
        public DbSet<WorkingDay> WorkingDay { get; set; }
        public DbSet<CurrencyName> CurrencyNames { get; set; }
        public DbSet<WeightName> WeightNames { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<Department> Departments { get; set; }
        //public DbSet<PriceWidthFoods> PriceWidthFoods { get; set; }
        //public System.Data.Entity.DbSet<ProjectAmur.Models.ApplicationUser> ApplicationUsers { get; set; }

    }
}