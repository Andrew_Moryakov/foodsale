﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using ProjectAmur;
using ProjectAmur.Models;

namespace ProjectAmur.Models
{
    public class DbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        private enum AmurRoles
        {
            Administrator,
            User,
            Debuger
        }


        protected override void Seed(ApplicationDbContext context)
        {
            //var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));


            //// создаем три роли
            //var role1 = new IdentityRole {Name = AmurRoles.Administrator.ToString()};
            //var role2 = new IdentityRole { Name = AmurRoles.User.ToString() };
            //var role3 = new IdentityRole { Name = AmurRoles.Debuger.ToString() };

            //// добавляем роли в бд
            //roleManager.Create(role1);
            //roleManager.Create(role2);
            //roleManager.Create(role3);

            //// создаем пользователей
            //var admin = new ApplicationUser
            //{
            //    Email = "DEBUGER@gmail.com",
            //    UserName = "Andrew_Moryakov",
            //     Name = "Andrew",
            //    SurName = "Moryakov"
            //};
            //const string password = "123456";
            //var result = userManager.Create(admin, password);

            //// если создание пользователя прошло успешно
            //if (result.Succeeded)
            //{
            //    // добавляем для пользователя роль
            //    userManager.AddToRole(admin.Id, role1.Name);
            //    userManager.AddToRole(admin.Id, role2.Name);
            //    userManager.AddToRole(admin.Id, role3.Name);
            //}

       
                var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

                // создаем три роли
                var role1 = new IdentityRole { Name = AmurRoles.Administrator.ToString() };
                var role2 = new IdentityRole { Name = AmurRoles.User.ToString() };
                //var role3 = new IdentityRole {Name = AmurRoles.Debuger.ToString()};

                // добавляем роли в бд
                roleManager.Create(role1);
                roleManager.Create(role2);
                //roleManager.Create(role3);

                var user = new ApplicationUser
                {
                    Actual = true,
                     Email = "DEBUGER@gmail.com",
                    UserName = "DEBUGER@gmail.com",
                    Name = "Andrew",
                    SurName = "Moryakov"
                };
              var result = userManager.Create(user, "123456");
                if (result.Succeeded)
                {
                    userManager.AddToRole(user.Id, role1.Name);
                    userManager.AddToRole(user.Id, role2.Name);
                }
                base.Seed(context);
          

        }
    }
}
