﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAmur.Models.Helpers
{
    public class OrderTwo
    {
        //                        DiscountBool: foodel.Category.DiscountBool, //Возможна ли скидка для этого блюда в прнципе
        //                        DiscountSize: foodel.Category.Discount,
        //                        Category: foodel.Category.Id,
        //                        FoodId: foodel.Id,
        //                        foodTitle: foodel.Title,
        //                        PriceId: foodel.PriceWidthsKey,
        //                        PriceSelected: "",
        //                        Price: foodel.PriceWidths, // $scope.PriceOfFood(foodel),
        //                        Number: foodel.Number,
        //                        Discount: $scope.isDiscount, //Включили ли мы скидку для текущего заказа
        //                        PriceWidthsKey: foodel.PriceWidthsKey,
        //                        PriceDiscount: ""

        
        public int FoodId { get; set; }
        public String FoodTitle { get; set; }
        public String PriceWidthsKey { get; set; }
        public Double PriceSelected { get; set; }
        public int Number { get; set; }
        public bool Discount { get; set; }
        public Double PriceDiscount { get; set; }
        public Boolean DiscountBool { get; set; }
        public Double DiscountSize { get; set; }
        public Category Category { get; set; }
        public int PriceId { get; set; }
        public int? Department { get; set; }
        public List<PriceWidth> Price { get; set; }
    }

    public class Orders
    {
        public String Key { get; set; }
        public String User { get; set; }
        public int TableId { get; set; }
        public List<OrderTwo> Data { get; set; }
    }
    class HelperModel
    {
        
        public class Order
        {
            public Order(String key, List<Food> foodName, Int32 number, Category category)
            {
                Key = key;
                Foods = foodName;
                foreach (var item in Foods)
                {
                    item.PriceWidths = item.PriceWidths.Where(el => el.Actual).ToList();
                }
                //Prices = prices;
                Number = number;
                Category = category;
            }
            public Category Category { get; set; }
            public String Key { get; set; }
            public List<Food> Foods { get; set; }
            //public List<PriceSelected> Prices { get; set; }
            public int Number { get; set; }
        }

        //public class Orders
        //{
        //    public List<Order> OrdersCollect { get; set; } 
        //}

        public class Price
        {
            public Price(PriceWidth priceWidth)
            {
                PriceId = priceWidth.Id;
                PriceName = priceWidth.Price;
            }
            public int PriceId { get; set; }
            public Double PriceName { get; set; }
        }
    }
}
