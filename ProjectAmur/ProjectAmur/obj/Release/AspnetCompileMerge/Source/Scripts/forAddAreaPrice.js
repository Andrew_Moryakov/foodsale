﻿    function CustomerEdit() {
        var _this = this;
        this.ajaxAddIco = "/Foods/ReturnAreaPriceWidth";

        this.init = function () {
            $("#ButtonAdd").click(function () {
                $.ajax({
                    type: "POST",
                    url: _this.ajaxAddIco,
                    success: function (data) {
                        $("#PriceList").append(data);
                        $("#CreateBtn").show();
                    }
                });
            });

            $(document).on("click", ".remove-line", function () {
                $(this).closest("#PriceWidthContent").remove();
            });
        }
    }

var customerEdit = null;
$(document).ready(function () {
    customerEdit = new CustomerEdit();
    customerEdit.init();
});