using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using ProjectAmur.Controllers.Helpers;
using ProjectAmur.Models;
using WebGrease.Css.Extensions;

namespace ProjectAmur.Controllers
{
    public class UserController : Controller
    {
        [Authorize(Roles = "User")]
        public async Task<ActionResult> Index()
        {
            return View();
        }

        private readonly ApplicationDbContext db = new ApplicationDbContext();
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //private string RenderRazorViewToString(string viewName, object model)
        //{
        //    ViewData.Model = model;
        //    using (var sw = new StringWriter())
        //    {
        //        var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
        //                                                                 viewName);
        //        var viewContext = new ViewContext(ControllerContext, viewResult.View,
        //                                     ViewData, TempData, sw);
        //        viewResult.View.Render(viewContext, sw);
        //        viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
        //        return sw.GetStringBuilder().ToString();
        //    }
        //}



        ///// <summary>
        ///// ������ 
        ///// </summary>
        ///// <param name="table"></param>
        ///// <param name="orders"></param>
        ///// <returns></returns>
        //public ActionResult SendOrder(String table, List<NumberFood> orders)
        //{
        //    var partialString = RenderRazorViewToString("~/Views/Categories/CategoryPart.cshtml", orders);

        //    return Content(partialString);
        //}


        //[HttpPost]
        //public ActionResult OkOrder(OrderCollection model)
        //{
        //    ModelCafeHelper.OrderHelper helper = new ModelCafeHelper.OrderHelper();
        //    if (helper.WriteInvoce(model, User))
        //    {
        //        return PartialView("~/Views/Alerts/Alert.cshtml", model.Numbers.Values.Sum(el => el.Number).ToString());
        //    }
        //    else
        //    {
        //        return PartialView("~/Views/Alerts/AlertError.cshtml",
        //            "�� ������� ���������� �����: ");
        //    }
        //}

        //public ActionResult OnSuccessOrder(OrderCollection model)
        //{
        //    if (model.Numbers.Values.All(el => el.Number < 0))//���� � ������ ���� ������������� ��������
        //        return PartialView("~/Views/Alerts/AlertError.cshtml",
        //            "���������� ���� �� ����� ���� ������������ ");


        //    if (Request.IsAjaxRequest())
        //    {
        //        ModelCafeHelper.OrderHelper helper = new ModelCafeHelper.OrderHelper();
        //        try
        //        {
        //            model.Numbers = model.Numbers.Where(el => el.Value.Number > 0).ToDictionary(el => el.Key, el => el.Value);//��������� ������ �� �����, ���������� ������� ������ 0
        //            if (!model.Numbers.Any())//���� ��� ��������� ���� � ���������� 0
        //                return PartialView("~/Views/Alerts/AlertError.cshtml",
        //                    "������ ��� ��������� �����, �������� ������ ���� ����� ");

        //            helper.WriteInvoce(model, User);//���������� � ����
        //            int count = model.Numbers.Values.Sum(item => item.Number);
        //            return PartialView("../Alerts/Alert", Convert.ToString(count));
        //        }
        //        catch (Exception ex)
        //        {
        //            return PartialView("../Alerts/AlertError",
        //                ex.Message + "*** ����" + ex.HelpLink + "*** ����" + Environment.NewLine + ex.StackTrace);
        //        }
        //    }
        //    return PartialView("~/Views/Alerts/AlertError.cshtml",
        //            "������ �� ������: ");
        //}
    }
}