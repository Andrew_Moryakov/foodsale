﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectAmur.Models;

namespace ProjectAmur.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CurrencyNamesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CurrencyNames
        public async Task<ActionResult> Index()
        {
            return View(await db.CurrencyNames.ToListAsync());
        }

        // GET: CurrencyNames/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CurrencyName currencyName = await db.CurrencyNames.FindAsync(id);
            if (currencyName == null)
            {
                return HttpNotFound();
            }
            return View(currencyName);
        }

        // GET: CurrencyNames/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CurrencyNames/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Prefix")] CurrencyName currencyName)
        {
            if (ModelState.IsValid)
            {
                db.CurrencyNames.Add(currencyName);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(currencyName);
        }

        // GET: CurrencyNames/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CurrencyName currencyName = await db.CurrencyNames.FindAsync(id);
            if (currencyName == null)
            {
                return HttpNotFound();
            }
            return View(currencyName);
        }

        // POST: CurrencyNames/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Prefix")] CurrencyName currencyName)
        {
            if (ModelState.IsValid)
            {
                db.Entry(currencyName).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(currencyName);
        }

        // GET: CurrencyNames/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CurrencyName currencyName = await db.CurrencyNames.FindAsync(id);
            if (currencyName == null)
            {
                return HttpNotFound();
            }
            return View(currencyName);
        }

        // POST: CurrencyNames/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CurrencyName currencyName = await db.CurrencyNames.FindAsync(id);
            db.CurrencyNames.Remove(currencyName);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
