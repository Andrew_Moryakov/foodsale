﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using ProjectAmur.Controllers.Helpers;
using ProjectAmur.Images;
using ProjectAmur.Models;
using ProjectAmur.Models.Helpers;
using SQLite;
using WebGrease.Css.Extensions;
using Table = ProjectAmur.Models.Table;

namespace ProjectAmur.Controllers
{
    public class FsApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public HttpResponseMessage ToJson(String json)
        {
            string yourJson = json;
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(yourJson, Encoding.UTF8, "application/json");
            return response;
        }


        private String CloseWorDay(WorkDayStatus status, DateTime createInvoice = new DateTime())
        {
            if(!KeyValidation(status.Key))
            {
                return "open";
            }
            if (createInvoice < new DateTime(1970, 01, 01))
            {
                createInvoice = DateTime.Now;
            }

            String st = "open";
            using (var db = new ApplicationDbContext())//Close Day, save incom
            {
                var openWorkDay =//Записываем сюда рабочий день, который не закрыт, но открыт и открыл его пользователь,с Ид который авторизирован или получен как параметр АПИ
                     db.WorkingDay.FirstOrDefault(wdEl => wdEl.ClosingDay == null && wdEl.UserId == status.User);//вытаскиваем Ид из БД, чтобы сравнить его с записью о рабочем дне
                if (openWorkDay != null)
                {
                    openWorkDay.ClosingDay = createInvoice;//Закрываем день
                   // var invoices = db.Invoises.Include(el => el.PriceWidth).Include(el => el.WorkingDay).Where(el => el.WorkingDay.Id == openWorkDay.Id);//Получаем проданные блюда между открытием и закрытием дня этим пользователем
                    //if (invoices.Any())//если были продажи
                    //{
                    //    openWorkDay.Income = invoices.Select(el => (el.PriceWidth.Price * el.Number) - el.Discount).Sum(el => el);//Считаем доход сделанные этим пользователем
                    //}
                    var usr = db.Users.FirstOrDefault(el => el.Id == status.User);
                    if(usr != null)
                    {
                        usr.RandomKey = null;
                        db.Entry(usr).State = EntityState.Modified; //Фиксируем
                    }
                    else
                    {
                        st = "open";
                        return st;
                    }
                    db.Entry(openWorkDay).State = EntityState.Modified;//Фиксируем
                    db.SaveChanges();//Сохраняем изменения
                    st = "close";

                }//Close Open Day


            }
            return st;
        }
        [System.Web.Http.HttpPost]
        public String WorkDayOut(WorkDayStatus status)
        {
            return CloseWorDay(status);
        }
        
        public class WorkDayStatus
        {
            public WorkDayStatus()
            {
                
            }
            public WorkDayStatus(String key, String user)
            {
                Key = key;
                User = user;
            }
            public String Key { get; set; } = "";
            public String Status { get; set; } = "";
            public String User { get; set; } = "";
        }
        [System.Web.Http.HttpPost]
        public async Task<HttpResponseMessage> WorkDayIn(WorkDayStatus status)
        {//Надо бы сделать восстановление состояния заказа, при подключении днного официанта с другого устройства 
            SetOfStatusMess setStatus = new SetOfStatusMess();
            ApiHelpers helper = new ApiHelpers();
            ModelCafeHelper.UserHelper userHelper = new ModelCafeHelper.UserHelper();
            Random rnd = new Random();
            using (var db = new ApplicationDbContext())
            {
                var tempUser = db.Users.FirstOrDefaultAsync(userEl => userEl.Actual && userEl.RandomKey==status.Key).Result;//Ищем пользователя в БД 

                var workDay = await db.WorkingDay.Include(incl => incl.Employee).FirstOrDefaultAsync(el => el.Employee.Email == tempUser.Email && el.ClosingDay == null && el.OpeningDay != null);//Ищем связанную с этим пользователем не закрытую смену
                if(workDay == null && tempUser!=null)//Если нет незакрытых
                { //Будем открывать новую смену для юзера
                    try
                    {
                        db.WorkingDay.Add(new WorkingDay
                        {
                            OpeningDay = DateTime.Now,
                            Employee = tempUser
                        });
                        db.SaveChanges();
                        var statusUser = userHelper.AddRandomKey(tempUser.Id);//Добавляем юзеру в базу рандомный ключ доступа
                        //Создали смену
                        status.Key = statusUser.Key;//tempUser.PasswordHash + helper.CalculateMD5Hash(tempUser.RandomKey);//Ключ
                        status.Status = setStatus.wDSuccessOpen;//И результат работы
                    }
                    catch
                    {
                        status.Status = setStatus.errOpenWDErrDB;//Смена не сохранена и не закрыта
                    }
                } //Close Open Day
                else
                if(workDay != null && tempUser != null)
                {//Если есть открытая и не закрытая смена связанная с текущим юзером 
                    if(status.Key == tempUser.RandomKey)//tempUser.PasswordHash + helper.CalculateMD5Hash(tempUser.RandomKey))
                                                        //Проверяем полученный ключ
                    {
                        //Таким образом пускаем только пользователей из базы и только тех, кто открыл смену, если зайти с другого устройства, то доступа не будет. МОжно сделать оповещение у первого пользователя и возможность подтерждения получения доступа к смене для этого пользователя с другого устройства
                        status.Status = setStatus.wDSuccessOpen;
                    }
                    else
                    {
                        status.Status = setStatus.notIdentifiKey;
                    }
                }


                return ToJson(JsonConvert.SerializeObject(status));
            }
        }

      

        [System.Web.Http.HttpPost]
        public async Task<HttpResponseMessage> GetTables(WorkDayStatus status)
        {
            SetOfStatusMess setStatus = new SetOfStatusMess();
            String resltJSON = JsonConvert.SerializeObject(new WorkDayStatus
            {
                Key = "",
                Status = setStatus.notIdentifiKey
            });
            if (!String.IsNullOrEmpty(status.Key) && KeyValidation(status.Key))//Если не пустая строка и ключ подходит
            {
                ApiHelpers helper = new ApiHelpers();
               
                using(var db = new ApplicationDbContext())
                {
                    ApplicationUser tempUser =
                        db.Users.FirstOrDefaultAsync(el => el.Actual && el.Email == User.Identity.Name).Result;
                        //текущий юзер

                    if( status.Key == tempUser.RandomKey)//tempUser.PasswordHash + helper.CalculateMD5Hash(tempUser.RandomKey)) //Если переданный ключ == хэш пароля + хэш временного ключа
                    {
                        resltJSON =
                            JsonConvert.SerializeObject(
                                await db.Tables.Where(el => el.Actual && el.Visible).ToListAsync());
                    }
                   
                }
            }
            return ToJson(resltJSON);
        }

        
        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetCategoriesAndFoods(WorkDayStatus status) //String table)
        {
            #region old
            //var helper = new OtherHelpers();
            //var keysFoods = helper.GetKeysFoods(); //Блюда, которые будут добавлены к категории.
            //using (var db = new ApplicationDbContext())
            //{
            //    IEnumerable<Food> foods = db.Foods.Include(el => el.Category).Include(el => el.PriceWidths); 
            //    List<HelperModel.Order> orders = new List<HelperModel.Order>();
            //    foods.ForEach(food => food.PriceWidths.ForEach(price => price.WeightName = db.WeightNames.FirstOrDefault(wn => wn.Id == price.WeightNameId)));
            //    foods.ForEach(food => food.PriceWidths.ForEach(price => price.CurrencyName = db.CurrencyNames.FirstOrDefault(сn => сn.Id == price.CurrencyNameId)));

            //    foreach (var item in foods.Where(foodEl => orders.All(el => el.Category.Id != foodEl.Category.Id) && foodEl.Actual && foodEl.Category.Actual && foodEl.Category.Visible && foodEl.Visible))//Извлекаем из БД те блюда, которые ещё не добавили в список для отправки
            //    {
            //        orders.Add(
            //            new HelperModel.Order(Guid.NewGuid().ToString("N"),//Добавляем ключ
            //            foods.Where(elfood => elfood.Category.Id == item.Category.Id && elfood.Actual && elfood.Visible).ToList(),//Добавляем коллекцию блюд, которые есть в текущей категории
            //            0,//Количество блюд
            //            item.Category)//Категория
            //            );
            //    }
            //    String resltJson = JsonConvert.SerializeObject(orders);
            #endregion

            SetOfStatusMess setStatus = new SetOfStatusMess();
            String resltJSON = JsonConvert.SerializeObject(new WorkDayStatus
            {
                Key = "",
                Status = setStatus.notIdentifiKey
            });

            if (!String.IsNullOrEmpty(status.Key) && KeyValidation(status.Key)) //Если не пустая строка и ключ подходит
            {
                Orders orders = new Orders
                {
                    Data = new List<OrderTwo>(),
                    TableId = -1
                };
                using(var db = new ApplicationDbContext())
                {
                    var firstOrDefault = db.Users.FirstOrDefault(user => user.Email == User.Identity.Name);
                    if(firstOrDefault != null)
                    {
                        orders.User = firstOrDefault.Id;
                    }
                    else
                    {
                        SetOfStatusMess mes = new SetOfStatusMess();
                        return ToJson(JsonConvert.SerializeObject(mes.userNotFound));
                        ;
                    }
                    var foods =db.Foods.Include(el => el.Category).Include(el => el.PriceWidths).Where( food => food.Category.Actual && food.Category.Visible && food.Actual && food.Visible && food.Category != null );
                    foreach(var food in foods)
                    {
                        orders.Data.Add(new OrderTwo
                        {
                            FoodId = food.Id,
                            Category = food.Category,
                            DiscountSize = food.Category.Discount,
                            DiscountBool = food.Category.DiscountBool,
                            FoodTitle = food.Title,
                            Number = 0,
                            Price = food.PriceWidths.Where(pr => pr.Actual).ToList(),
                            PriceWidthsKey = Convert.ToString(food.PriceWidths.FirstOrDefault().Id),
                            Department = food.Department?.Id ?? null
                        });
                    }

                    orders.Data.ForEach(food =>food.Price.ForEach(price => price.WeightName = db.WeightNames.FirstOrDefault(wn => wn.Id == price.WeightNameId)));
                    orders.Data.ForEach(food =>food.Price.ForEach(price => price.CurrencyName = db.CurrencyNames.FirstOrDefault(сn => сn.Id == price.CurrencyNameId)));

                    resltJSON = JsonConvert.SerializeObject(orders);
                }

            }
            return ToJson(resltJSON);
            
        }

        private bool KeyValidation(String key)
        {

            using (var db = new ApplicationDbContext())
            {
                var firstOrDefault = db.Users.FirstOrDefault(el => el.Actual && el.RandomKey==key);
                if (firstOrDefault != null)
                {
                    return true;
                }
                return false;
            }
        }
        
             [System.Web.Http.HttpPost]
        public string GetCurrentWorkDaySum(Orders data) //String table)
             {
                 using(var db = new ApplicationDbContext())
                 {


                     var reslt =
                         db.Invoises.Include(el => el.WorkingDay).Include(el=>el.PriceWidth).Where(
                             el => el.WorkingDay.ClosingDay == null && el.WorkingDay.Employee.Id == data.User).ToList().Select(
                                 el => (el.Price == -1 ? el.PriceWidth.Price * Double.Parse(el.Number) : el.Price * Double.Parse(el.Number))).Sum().ToString();

                     return reslt;
                 }
             }

        [System.Web.Http.HttpPost]
        public List<string> PostOrder(Orders data) //String table)
        {

                ModelCafeHelper.OrderHelper helper = new ModelCafeHelper.OrderHelper();
            if(data != null && data.Data.Any())
            {
                try
                {
                    helper.WriteInvoce(data);
                }
                catch (Exception ex)
                {
                    return new List<string>()
                           {
                               "1",
                               "mes:" + ex.Message + "sourse:"+ex.Source
                           };
                }
            }
            return  new List<string>()
                           {
                               "0",
                               "mes:"
                           }; ;
        }

        public class RecoveryOrders : Orders
        {
            public String Password { get; set; }
            public String Date { get; set; }
            public String Time { get; set; }
        }


        [System.Web.Http.HttpGet]
        public Boolean Password(String Password)
        {
            return true;
        }

        [System.Web.Http.HttpPost]
        public Boolean RecoveryLostOrders(Dictionary<string, string> data)
        {

            #region SqlIteToSQLCompact
            //using (var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\menu.sqlite"))
            //{
            //    List<Category> categories = new List<Category>();
            //    using (ApplicationDbContext db = new ApplicationDbContext())
            //    {

            //        db.CurrencyNames.Add(new CurrencyName
            //        {
            //            Name = "Рубль",
            //            Prefix = "руб."
            //        });

            //        db.WeightNames.Add(new WeightName
            //        {
            //            Name = "Грамм",
            //            Prefix = "гр."
            //        });
            //        db.SaveChanges();
            //        foreach (var item in liteDb.Table<category>())
            //        {
            //            categories.Add(new Category
            //            {
            //                Actual = true,
            //                Visible = true,
            //                Discount = 5,
            //                DiscountBool = true,
            //                Title = item.title
            //            });
            //        }
            //        db.Categories.AddRange(categories);
            //        db.SaveChanges();
            //        foreach (var liteFood in liteDb.Table<food>())
            //        {
            //            List<PriceWidth> pws = new List<PriceWidth>();
            //            if (liteFood.amount2 != null)
            //            {
            //                pws.AddRange(new List<PriceWidth>
            //                {
            //                    new PriceWidth
            //                    {
            //                        Actual = true,
            //                        CurrencyNameId = db.CurrencyNames.FirstOrDefault().Id,
            //                        WeightNameId = db.WeightNames.FirstOrDefault().Id,
            //                        DataChange = DateTime.Now,
            //                        Price = Convert.ToDouble(liteFood.price),
            //                        Width = Convert.ToString(liteFood.amount)
            //                    },
            //                    new PriceWidth
            //                    {
            //                        Actual = true,
            //                        CurrencyNameId = db.CurrencyNames.FirstOrDefault().Id,
            //                        WeightNameId = db.WeightNames.FirstOrDefault().Id,
            //                        DataChange = DateTime.Now,
            //                        Price = Convert.ToDouble(liteFood.price2),
            //                        Width = Convert.ToString(liteFood.amount2)
            //                    }
            //                });
            //                ;

            //            }
            //            else
            //            {
            //                pws.AddRange(new List<PriceWidth>
            //                {
            //                    new PriceWidth
            //                    {
            //                        Actual = true,
            //                        DataChange = DateTime.Now,
            //                         CurrencyNameId = db.CurrencyNames.FirstOrDefault().Id,
            //                        WeightNameId = db.WeightNames.FirstOrDefault().Id,
            //                        Price = Convert.ToDouble(liteFood.price),
            //                        Width = Convert.ToString(liteFood.amount)
            //                    }
            //                });


            //            }

            //            foreach (var item in pws)
            //            {
            //                db.PriceWidths.Add(item);
            //            }


            //            db.Foods.Add(new Food
            //            {
            //                Actual = true,
            //                Visible = true,
            //                Title = liteFood.title,
            //                PriceWidths = pws,
            //                CategoryId = categories.FirstOrDefault(el => el.Id == liteFood.category_id).Id,
            //                Description = liteFood.description

            //            });
            //        }
            //        db.SaveChanges();
            //    }
            //}
            #endregion
            try
            {
                if(data.Any())
                { 
                    ModelCafeHelper.OrderHelper helper = new ModelCafeHelper.OrderHelper();
                    IEnumerable<RecoveryOrders> valuesData = data.Values.Select(JsonConvert.DeserializeObject<RecoveryOrders>).Where(order => order != null);
                    foreach(KeyValuePair<string, string> order in data)
                    {
                        
                        if(order.Key.IndexOf("fsos", StringComparison.Ordinal) > -1)
                        {
                            RecoveryOrders objData = JsonConvert.DeserializeObject<RecoveryOrders>(order.Value);
                            helper.WriteInvoce(objData, Convert.ToDateTime(objData.Date + " " + objData.Time));
                        }
                    }
                    foreach(KeyValuePair<string, string> order in data)
                    {
                     
                        if (order.Key.IndexOf("fswo", StringComparison.Ordinal) > -1)
                        {
                            RecoveryOrders objData = JsonConvert.DeserializeObject<RecoveryOrders>(order.Value);
                            CloseWorDay(new WorkDayStatus(objData.Key, objData.User), Convert.ToDateTime(objData.Date + " " + objData.Time));
                        }
                    }
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
