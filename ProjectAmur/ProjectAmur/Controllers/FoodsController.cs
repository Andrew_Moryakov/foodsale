﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectAmur.Controllers.Helpers;
using ProjectAmur.Models;
using WebGrease.Css.Extensions;

namespace ProjectAmur.Controllers
{
    public class FoodsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult ReturnAreaPriceWidth()
        {
        ViewBag.CurrencyNames = new SelectList(db.CurrencyNames, "Id", "Name");
            ViewBag.WeightNames = new SelectList(db.WeightNames, "Id", "Name");
            return View("PriceWidthArea", new KeyValuePair<string, PriceWidth>(Guid.NewGuid().ToString("N"),
                new PriceWidth()));
             
        }


        [Authorize(Roles = "Administrator")]
        // GET: Foods
        public ActionResult Index( )
        {
            var foods = db.Foods.Include(f => f.Category).Include(elPrice => elPrice.PriceWidths).Where(food=>food.Actual && food.Visible);
            foods.ForEach(food => food.PriceWidths.ForEach(price => price.WeightName = db.WeightNames.FirstOrDefault(wn => wn.Id == price.WeightNameId)));
            foods.ForEach(food => food.PriceWidths.ForEach(price => price.CurrencyName = db.CurrencyNames.FirstOrDefault(сn => сn.Id == price.CurrencyNameId)));
            foreach (var item in foods)
            {
                item.PriceWidths = item.PriceWidths.Where(el => el.Actual).ToList();
            }
            return View( foods );
        }

        [Authorize(Roles = "Administrator")]
        // GET: Foods/Details/5
        public async Task<ActionResult> Details( int? id )
        {
            if (id == null)
            {
                return new HttpStatusCodeResult( HttpStatusCode.BadRequest );
            }
            
            Food food = await db.Foods.Include(el=>el.Category).Include(elP=>elP.PriceWidths).FirstAsync(el=>el.Id==id);
            if (food == null)
            {
                return HttpNotFound();
            }
            return View( food );
        }


        [Authorize(Roles = "Administrator")]
        // GET: Foods/Create
        public ActionResult Create( )
        {
            var firstOrDefault = db.CurrencyNames.FirstOrDefault();
            if(firstOrDefault != null)
            {
                ViewBag.CurrencyNames = new SelectList(db.CurrencyNames, "Id", "Name", firstOrDefault.Id);
            }
            var weightName = db.WeightNames.FirstOrDefault();
            if(weightName != null)
            {
                ViewBag.WeightNames = new SelectList(db.WeightNames, "Id", "Name", weightName.Id);
            }
            ViewBag.CategoryId = new SelectList( db.Categories, "Id", "Title" );
            return View();
    


        }
        
        // POST: Foods/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> Create( Food food )//[Bind(Include = "Id,Title,Image,CategoryId,Description")] Food food)
        {
            if (ModelState.IsValid)
            {
                var helper = new ModelCafeHelper.FoodHelper();
                await helper.AddFoods(food, Server);

                return RedirectToAction( "Index" );
      
            }
            ViewBag.CategoryId = new SelectList( db.Categories, "Id", "Title", food.CategoryId );
            return View( food );
        }


        [Authorize(Roles = "Administrator")]
        // GET: Foods/Edit/5
        public async Task<ActionResult> Edit( int? id )
        {
            if (id == null)
            {
                return new HttpStatusCodeResult( HttpStatusCode.BadRequest );
            }
           

            Food food = await db.Foods.Include(elC=>elC.Category).Include( elPS => elPS.PriceWidths).FirstAsync( elP => elP.Id == id );

            foreach (var item in food.PriceWidths.Where(item => item.Actual))
            {
                food.PriceWidthsKey.Add(Guid.NewGuid().ToString("N"), item);
            }

            if (food == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.CategoryId = new SelectList( db.Categories, "Id", "Title"/*, food.CategoryId*/ );

            ViewBag.CurrencyNames = new SelectList(db.CurrencyNames, "Id", "Name"/*, food.PriceWidths.FirstOrDefault().CurrencyName*/);
            ViewBag.WeightNames = new SelectList(db.WeightNames, "Id", "Name"/*, food.PriceWidths.FirstOrDefault().WeightNameId*/);

            return View( food );
        }

        // POST: Foods/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> Edit( Food food )
        {
                if (ModelState.IsValid)
            {
                food.Actual = true;
                food.Visible = true;

                food.PriceWidths.ForEach( el => el.DataChange = DateTime.UtcNow );//Каждой, только что добавленной, цене назначаем текущее время добавления
                Food thisFood = food;
                if (food.PriceWidthsKey.Values.Any())
                {
                      thisFood = db.Foods.Include(el => el.PriceWidths).First(el => el.Id == food.Id); //Создаем копию этого блюда с связанными ценами

                    foreach (var item in thisFood.PriceWidths)
                    {//Отключаем в базе цены у этого блюда
                        item.Actual = false;
                    }

                    thisFood.Title = food.Title;
                    thisFood.DepartmentId = food.DepartmentId;
                    thisFood.Description = food.Description;
                    thisFood.Actual = food.Actual;
                    thisFood.Visible = food.Visible;
                    thisFood.CategoryId = food.CategoryId;
                    foreach (var itemNewPrice in food.PriceWidthsKey.Values)
                    {
                        thisFood.PriceWidths.Add(new PriceWidth
                        {//Создаём для копии блюда, из БД, новую цену
                            CurrencyNameId = itemNewPrice.CurrencyNameId,
                            WeightNameId = itemNewPrice.WeightNameId,
                            Price = itemNewPrice.Price,
                            Width = itemNewPrice.Width,
                            DataChange = DateTime.UtcNow,
                            Actual = true
                        });
                    }
                }
                var helper = new ModelCafeHelper();
                thisFood.ImagePath = helper.GetAndSaveImageOnServer( food.ImageFile.First(), Server, "~/Images/Food/" );
                db.Entry( thisFood ).State = EntityState.Modified;
                await db.SaveChangesAsync();

                return RedirectToAction( "Index" );
            }
            ViewBag.CategoryId = new SelectList( db.Categories, "Id", "Title", food.CategoryId );
            return View( food );
        }


        [Authorize(Roles = "Administrator")]
        // GET: Foods/Delete/5
        public async Task<ActionResult> Delete( int? id )
        {
            if (id == null)
            {
                return new HttpStatusCodeResult( HttpStatusCode.BadRequest );
            }
            Food food = await db.Foods.FindAsync( id );
            if (food == null)
            {
                return HttpNotFound();
            }
            food.Actual = false;
            food.PriceWidths.ForEach(el=>el.Actual=false);
            db.Entry(food).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return View( food );
        }

        // POST: Foods/Delete/5
        [HttpPost, ActionName( "Delete" )]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> DeleteConfirmed( int id )
        {
            try
            {
                db.Invoises.Where(el => el.Food.Id == id).Include(el => el.Food).ForEach(el => el.FoodId = null);

                await db.SaveChangesAsync();
                Food food = await db.Foods.FindAsync(id);
                db.Foods.Remove(food);
                // db.Foods.Include(elCat=>elCat.Category).Where(el=>el.Category.)
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch
            {//Костыль
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose( bool disposing )
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose( disposing );
        }
    }
}
