﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectAmur.Models;

namespace ProjectAmur.Controllers
{
    public class ClientController : Controller
    {
        // GET: Client
        public ActionResult Index()
        {
            using (var db = new ApplicationDbContext())
            {
                ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title");
                return View();
            }
        }
    }
}