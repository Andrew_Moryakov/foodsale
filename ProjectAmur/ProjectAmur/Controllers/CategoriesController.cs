﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ProjectAmur.Controllers.Helpers;
using ProjectAmur.Models;
using WebGrease.Css.Extensions;

namespace ProjectAmur.Controllers
{
    public class CategoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> Index()
        {
            var categories = db.Categories.Include(c => c.ParentCategory).Where(el=>el.Actual && el.Visible);//подсоединяем подкатегории
            //ViewBag.CatigoriesIncome  = ;
            Dictionary<Category, Double> CatigoriesIncome = new Dictionary<Category, double>();
            foreach(var category in categories)
            {
                double? sum=0;
                var category1 = category;
                foreach(var food in db.Foods.Include(el => el.Category).Include(el => el.PriceWidths).Where(food => food.Actual && food.Visible && food.CategoryId == category1.Id))
                { 
                        var s = db.Invoises.ToList().Where(inv =>inv!=null && inv.FoodId!=null && inv.FoodId == food.Id)?.Where(foodEl => foodEl.PriceWidth!=null && foodEl.PriceWidth.Price != null && foodEl.PriceWidth.Actual)?.Select(inv => inv.PriceWidth.Price).Sum();
                    if(s != null)
                    {
                        sum += (double)s;
                    }
                    
                }
                CatigoriesIncome.Add(category1, Convert.ToDouble(sum));
            }
            return View(CatigoriesIncome);
        }

        // GET: Categories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = await db.Categories.Include(el=>el.ParentCategory).FirstAsync(el=>el.Id==id);

            if (category == null)
            {
                return HttpNotFound();
            }

            //category.ImagePath = Path.GetFileName( category.ImagePath );
            return View( category );
           
        }

        [Authorize(Roles = "Administrator")]
        // GET: Categories/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title");
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> Create(Category category)
        {
            if(category.Title != null)
            {
                var helper = new ModelCafeHelper.CategoryHelper();
                /*await*/
                helper.AddCategory(category, Server);

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Title", category.CategoryId);
            return View(category);
        }

        [Authorize(Roles = "Administrator")]
        // GET: Categories/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = await db.Categories.Include("ParentCategory").FirstOrDefaultAsync(el=>el.Id==id);
            if (category == null)
            {
           // category.
                return HttpNotFound();
            }

            //category.ImagePath =category.ImagePath;
          ViewBag.CategoryId = new SelectList(db.Categories.ToList(), "Id", "Title", category.CategoryId);
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> Edit(Category category)
        {
            if (ModelState.IsValid)
            {
                category.Actual = true;
                category.Visible = true;
                var helper = new ModelCafeHelper();
                if (String.IsNullOrEmpty(category.ImagePath))
                {
                    category.ImagePath = helper.GetAndSaveImageOnServer(category.ImageFile.First(), Server,
                        "~/Images/Category/");
                }
                db.Entry(category).State = EntityState.Modified;
                await db.SaveChangesAsync();


                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList( db.Categories, "Id", "Title", category.CategoryId );
            return View( category );
           
        }

        [Authorize(Roles = "Administrator")]
        // GET: Categories/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = await db.Categories.FindAsync(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            category.Actual = false;
            db.Entry(category).State = EntityState.Modified;
            await db.SaveChangesAsync();
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Category category = await db.Categories.FindAsync(id);
            List<Category> subCategory = db.Categories.Where(el => el.CategoryId == category.Id).ToList();//Категории, которые являются детьми той, которую удаляем
            List<Food> foods = db.Foods.Where(el => el.CategoryId == id).ToList();//Ещим блюда,которые находятся в этой категории

            subCategory.ForEach(el=>el.CategoryId=null);//Уничтажаем ссылки на вторичные ключи вложенных категорий
            foods.ForEach( el => el.CategoryId = null );//Уничтажаем ссылки на родительскую категорию у всех блюд

            db.Categories.Remove(category);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
