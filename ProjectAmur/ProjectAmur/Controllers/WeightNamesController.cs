﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectAmur.Models;

namespace ProjectAmur.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class WeightNamesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: WeightNames
        public async Task<ActionResult> Index()
        {
            return View(await db.WeightNames.ToListAsync());
        }

        // GET: WeightNames/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WeightName weightName = await db.WeightNames.FindAsync(id);
            if (weightName == null)
            {
                return HttpNotFound();
            }
            return View(weightName);
        }

        // GET: WeightNames/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WeightNames/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(WeightName weightName)
        {
            if (ModelState.IsValid)
            {
                db.WeightNames.Add(weightName);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(weightName);
        }

        // GET: WeightNames/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WeightName weightName = await db.WeightNames.FindAsync(id);
            if (weightName == null)
            {
                return HttpNotFound();
            }
            return View(weightName);
        }

        // POST: WeightNames/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(WeightName weightName)
        {
            if (ModelState.IsValid)
            {
                db.Entry(weightName).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(weightName);
        }

        // GET: WeightNames/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WeightName weightName = await db.WeightNames.FindAsync(id);
            if (weightName == null)
            {
                return HttpNotFound();
            }
            return View(weightName);
        }

        // POST: WeightNames/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            WeightName weightName = await db.WeightNames.FindAsync(id);
            db.WeightNames.Remove(weightName);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
