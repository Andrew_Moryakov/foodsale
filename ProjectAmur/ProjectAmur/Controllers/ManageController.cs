﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ProjectAmur.Controllers.Helpers;
using ProjectAmur.Models;
using WebGrease.Css.Extensions;

namespace ProjectAmur.Controllers
{
    [Authorize]
    [Authorize(Roles = "Administrator")]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ManageController( )
        {
        }

        public ManageController( ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
      
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult UserList( )
        {
            Dictionary<String, List<String>> userRoles = new Dictionary<string, List<String>>();
            List<ApplicationUser> users = new List<ApplicationUser>();
            using (var db = new ApplicationDbContext())
            {
                users = db.Users.Where(el=>el.Actual).ToList();
                //foreach(var user in db.Users)
                //{
                //    foreach(var role in db.Roles)
                //    {
                        
                //    }
                //  userRoles.Add(user.Email, db.Roles.Where(role=>role.Id==user.Roles.Where(el=>el.UserId==user.Id).Select(el=>el.RoleId)));
                //}
                ViewBag.Roles = db.Roles.ToList();
            }
            
            return View( users );
        }
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> DeleteUser( String id )
        {
            using (var db = new ApplicationDbContext())
            {
                ApplicationUser user = db.Users.Find( id );

                user.Actual = false;
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }

            return RedirectToAction( "UserList" );
        }
        private enum AmurRoles
        {
            Administrator,
            User,
            Debuger
        }

        [HttpGet] 
        [Authorize(Roles = "Administrator")]
        public ActionResult CreateWaiter()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> CreateWaiter( RegisterViewModel model )
        {
            using (var context = new ApplicationDbContext())
            {
                var roleManager = new RoleManager<IdentityRole>( new RoleStore<IdentityRole>( context ) );

                var role2 = new IdentityRole { Name = AmurRoles.User.ToString() };

                roleManager.Create( role2 );

                if (!ModelState.IsValid) View( model );
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    Name = model.Name,
                    SurName = model.SurName,
                    Actual = true
                };
                var result = await UserManager.CreateAsync( user, model.Password );
                if (result.Succeeded)
                {
                    //await SignInManager.SignInAsync( user, isPersistent: false, rememberBrowser: false );
                  
                    UserManager.AddToRole( user.Id, role2.Name );

                    return RedirectToAction( "Index", "Home" );
                }
                AddErrors( result );

            }

            // If we got this far, something failed, redisplay form
            return View( model );
        }

        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> Index( ManageMessageId? message )
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var userId = User.Identity.GetUserId();
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync( userId ),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync( userId ),
                Logins = await UserManager.GetLoginsAsync( userId ),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync( userId )
            };

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult GetRatingFoodsPartial(String key)
        {
            return ViewBag();
            //using (var db = new ApplicationDbContext())
            //{
            //    //return PartialView("~/Views/PartialView/_RatingFoods.cshtml", new List<Invoice>((
            //    Dictionary<int?, List<Invoice>> rating = db.Invoises
            //        .Include("Food")
            //        .Include("PriceWidth")
            //        .Include("Table")
            //        .Include("WorkingDay")//Подключаем связанные таблицы
            //        .ToList<Invoice>()
            //        .GroupBy(el => el.FoodId)//Груперуем по блюдам (ключ блюдо)
            //        .ToDictionary(el => el.Key, el => el.ToList())//Превращаем в словарь
            //        .OrderByDescending(el => el.Value.Count)//Сортируем словарь по количеству в нем элементов. То есть сверху самые часто продаваемые блюда
            //        .ToDictionary(el => el.Key, el => el.Value);//Опять в словарь

            //    var reatingList = new List<Invoice>();
            //    using(Aes myAes = Aes.Create())
            //    {
            //        Crypto crypto = new Crypto();
            //        foreach(var item in rating)
            //        {
            //            Double sumPrices = item.Value.Sum(el => el.PriceWidth.Price * double.Parse(crypto.DecryptStringFromBytes_Aes(el.Number, crypto.GetBytes(key), myAes.IV)))  - item.Value.Sum(el => el.Discount);
            //            item.Value.First().Number = item.Value.Sum(el => el.Number);
            //                //Суммируем количество продаж данного блюда (продажы из разных чеков);
            //            item.Value.First().PriceWidth.Price = sumPrices;
            //            reatingList.Add(item.Value.First());
            //                //Добавляем в коллекцию (в скольких чеках было то или иное блюдо)
            //        } //Таким образом показывает рейтинг по частоте продаж блюд в разных чеках.
            //    }
            //    return PartialView("~/Views/PartialView/_RatingFoods.cshtml", reatingList.OrderByDescending(el=>el.Number).ToList());//Таким образом выводит рейтинг по продажам в целом (сколько копий всего было продано)
            //}

        }
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult GetAllInvoices()
        {
            using (var db = new ApplicationDbContext())
            {
               
                return PartialView("~/Views/PartialView/_Analytics.cshtml",new List<Invoice>(db.Invoises
                    .Include("Food")
                    .Include("PriceWidth")
                    .Include("Table")
                    .Include("WorkingDay").Include("Employee")
                    ));//Таким образом выводит рейтинг по продажам в целом
            }

        }
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public ActionResult GetIncome(String from, String to)
        { 
            using (var db = new ApplicationDbContext())
            {
                List<WorkingDay> col = new List<WorkingDay>();
                if(!String.IsNullOrEmpty(from) && !String.IsNullOrEmpty(to))
                {
                    from.Replace('.', ':');

                    to.Replace('.', ':');
                    DateTime? to2 = DateTime.Parse(to).Date;
                    DateTime? from2 = DateTime.Parse(from).Date;
                    col =
                        new List<WorkingDay>(
                            //from invoise in
                                db.Invoises.Include("Food").Include("PriceWidth").Include("Table").Include("Employee")
                            //where System.Data.Entity.DbFunctions.TruncateTime(invoise.Date.Date) >= from2 || System.Data.Entity.DbFunctions.TruncateTime(invoise.Date.Date) <= to2
                            //select invoise.WorkingDay);

                    .Where(
                         el =>( el.Date.Year >= from2.Value.Year && el.Date.Month >= from2.Value.Month && el.Date.Day >= from2.Value.Day)
                        ||
                         (el.Date.Year <= to2.Value.Year && el.Date.Month <= to2.Value.Month && el.Date.Day <= to2.Value.Day)
                         ).Select(el => el.WorkingDay).ToList());
                }
                else
                {
                    col = db.WorkingDay.Include("Employee").ToList();
                }
                return PartialView("~/Views/PartialView/_Income.cshtml", col
                    //Entity не работает с датами
                    );//Таким образом выводит рейтинг по продажам в целом
            }

        }
        
        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> RemoveLogin( string loginProvider, string providerKey )
        {
            ManageMessageId? message;
            var result = await UserManager.RemoveLoginAsync( User.Identity.GetUserId(), new UserLoginInfo( loginProvider, providerKey ) );
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync( User.Identity.GetUserId() );
                if (user != null)
                {
                    await SignInManager.SignInAsync( user, isPersistent: false, rememberBrowser: false );
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction( "ManageLogins", new { Message = message } );
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber( )
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber( AddPhoneNumberViewModel model )
        {
            if (!ModelState.IsValid)
            {
                return View( model );
            }
            // Generate the token and send it
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync( User.Identity.GetUserId(), model.Number );
            if (UserManager.SmsService != null)
            {
                var message = new IdentityMessage
                {
                    Destination = model.Number,
                    Body = "Your security code is: " + code
                };
                await UserManager.SmsService.SendAsync( message );
            }
            return RedirectToAction( "VerifyPhoneNumber", new { PhoneNumber = model.Number } );
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication( )
        {
            await UserManager.SetTwoFactorEnabledAsync( User.Identity.GetUserId(), true );
            var user = await UserManager.FindByIdAsync( User.Identity.GetUserId() );
            if (user != null)
            {
                await SignInManager.SignInAsync( user, isPersistent: false, rememberBrowser: false );
            }
            return RedirectToAction( "Index", "Manage" );
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication( )
        {
            await UserManager.SetTwoFactorEnabledAsync( User.Identity.GetUserId(), false );
            var user = await UserManager.FindByIdAsync( User.Identity.GetUserId() );
            if (user != null)
            {
                await SignInManager.SignInAsync( user, isPersistent: false, rememberBrowser: false );
            }
            return RedirectToAction( "Index", "Manage" );
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber( string phoneNumber )
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync( User.Identity.GetUserId(), phoneNumber );
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null ? View( "Error" ) : View( new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber } );
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber( VerifyPhoneNumberViewModel model )
        {
            if (!ModelState.IsValid)
            {
                return View( model );
            }
            var result = await UserManager.ChangePhoneNumberAsync( User.Identity.GetUserId(), model.PhoneNumber, model.Code );
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync( User.Identity.GetUserId() );
                if (user != null)
                {
                    await SignInManager.SignInAsync( user, isPersistent: false, rememberBrowser: false );
                }
                return RedirectToAction( "Index", new { Message = ManageMessageId.AddPhoneSuccess } );
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError( "", "Failed to verify phone" );
            return View( model );
        }

        //
        // GET: /Manage/RemovePhoneNumber
        public async Task<ActionResult> RemovePhoneNumber( )
        {
            var result = await UserManager.SetPhoneNumberAsync( User.Identity.GetUserId(), null );
            if (!result.Succeeded)
            {
                return RedirectToAction( "Index", new { Message = ManageMessageId.Error } );
            }
            var user = await UserManager.FindByIdAsync( User.Identity.GetUserId() );
            if (user != null)
            {
                await SignInManager.SignInAsync( user, isPersistent: false, rememberBrowser: false );
            }
            return RedirectToAction( "Index", new { Message = ManageMessageId.RemovePhoneSuccess } );
        }

        //
        // GET: /Manage/ChangePassword
        [Authorize(Roles = "Administrator")]
        public ActionResult ChangePassword( )
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> ChangePassword( ChangePasswordViewModel model )
        {
            if (!ModelState.IsValid)
            {
                return View( model );
            }
            var result = await UserManager.ChangePasswordAsync( User.Identity.GetUserId(), model.OldPassword, model.NewPassword );
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync( User.Identity.GetUserId() );
                if (user != null)
                {
                    await SignInManager.SignInAsync( user, isPersistent: false, rememberBrowser: false );
                }
                return RedirectToAction( "Index", new { Message = ManageMessageId.ChangePasswordSuccess } );
            }
            AddErrors( result );
            return View( model );
        }

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword( )
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> SetPassword( SetPasswordViewModel model )
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddPasswordAsync( User.Identity.GetUserId(), model.NewPassword );
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync( User.Identity.GetUserId() );
                    if (user != null)
                    {
                        await SignInManager.SignInAsync( user, isPersistent: false, rememberBrowser: false );
                    }
                    return RedirectToAction( "Index", new { Message = ManageMessageId.SetPasswordSuccess } );
                }
                AddErrors( result );
            }

            // If we got this far, something failed, redisplay form
            return View( model );
        }

        //
        // GET: /Manage/ManageLogins
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> ManageLogins( ManageMessageId? message )
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await UserManager.FindByIdAsync( User.Identity.GetUserId() );
            if (user == null)
            {
                return View( "Error" );
            }
            var userLogins = await UserManager.GetLoginsAsync( User.Identity.GetUserId() );
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where( auth => userLogins.All( ul => auth.AuthenticationType != ul.LoginProvider ) ).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View( new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            } );
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin( string provider )
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult( provider, Url.Action( "LinkLoginCallback", "Manage" ), User.Identity.GetUserId() );
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback( )
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync( XsrfKey, User.Identity.GetUserId() );
            if (loginInfo == null)
            {
                return RedirectToAction( "ManageLogins", new { Message = ManageMessageId.Error } );
            }
            var result = await UserManager.AddLoginAsync( User.Identity.GetUserId(), loginInfo.Login );
            return result.Succeeded ? RedirectToAction( "ManageLogins" ) : RedirectToAction( "ManageLogins", new { Message = ManageMessageId.Error } );
        }

        protected override void Dispose( bool disposing )
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose( disposing );
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors( IdentityResult result )
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError( "", error );
            }
        }

        private bool HasPassword( )
        {
            var user = UserManager.FindById( User.Identity.GetUserId() );
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber( )
        {
            var user = UserManager.FindById( User.Identity.GetUserId() );
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion
    }
}