﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAmur.Controllers.Helpers
{
    public class SetOfStatusMess
    {
        /// <summary>
        /// Ошибка закрытия смены, связанная с проблеммой при записи в базу данных
        /// </summary>
        public String errOpenWDErrDB = "DberrClose";
        /// <summary>
        /// Если рабочая смена успешно открыта
        /// </summary>
        public String wDSuccessOpen = "WDOpen";
        /// <summary>
        /// Если рабочая смена успешно закрыта
        /// </summary>
        public String wDSuccessClose = "WDClose";
        /// <summary>
        /// Если полученный пользователь не найден в базе данных
        /// </summary>
        public String userNotFound = "UserNotFound";
        /// <summary>
        /// Если пользователь успешно прошел идентификацию
        /// </summary>
        public String identifiKeySuccess = "IdentifiKeySuccess";
        /// <summary>
        /// Если пользователь не прошел идентификацию
        /// </summary>
        public String notIdentifiKey = "NotIdentifiKey";
        /// <summary>
        /// Если пользователь не прошел идентификацию
        /// </summary>
        public String authorizationSuccessful = "AuthorizationSuccessful";
    }
}
