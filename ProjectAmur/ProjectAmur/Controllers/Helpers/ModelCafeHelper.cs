using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using ProjectAmur.Models;
using Table = ProjectAmur.Models.Table;
using ProjectAmur.Models.Helpers;

namespace ProjectAmur.Controllers.Helpers
{
    public class WorkDayStatus
    {
        public WorkDayStatus()
        {

        }
        public WorkDayStatus(String key, String user)
        {
            Key = key;
            User = user;
        }
        public String Key { get; set; } = "";
        public String Status { get; set; } = "";
        public String User { get; set; } = "";
    }

    public class ModelCafeHelper
    {
        public class OrderHelper
        {


            /// <summary>
            /// ���������� ���������� � ������� � ���� ������
            /// </summary>
            /// <param name="model">����� ����, ���������� � ������� ����� �������� � ���� ������</param>
            /// <param name="createInvoice">����� �������� ����</param>
            /// <returns>���� �������� �������, ������ ������</returns>
            public async void WriteInvoce(Orders model, DateTime createInvoice = new DateTime())
            {

                if(createInvoice < new DateTime(1970, 01, 01))
                {
                    createInvoice = DateTime.Now;
                }

                using(var db = new ApplicationDbContext())
                {

                    Food foodFromDB;
                    var workingDay =
                        await
                            db.WorkingDay.Include(incl => incl.Employee).FirstOrDefaultAsync(
                                el =>
                                    el.ClosingDay == null
                                        && el.Employee.Email
                                            == db.Users.FirstOrDefault(userEl => userEl.Id == model.User).Email);
                    if(workingDay == null)
                    {
                        return;
                    }


                    //    Crypto crypto = new Crypto();
                    foreach(var order in model.Data)
                    {
                        var order1 = order;

                        if(order1.FoodId == -1)
                        {
                            Invoice invoice = new Invoice
                            { 
                                Discount = false,
                                //==true ? ((foodFromDB.PriceWidths.FirstOrDefault(priceEl=>priceEl.Actual && priceEl.Id==int.Parse//���� �� ��
                                //(order1.PriceWidthsKey)).Price * order1.Number) * foodFromDB.Category.Discount)/ 100 : 0,//�������� ���� �� ���������� � ������ ������/ �������� ������� �� �����
                                Price = order1.Number * order.Price.FirstOrDefault().Price,
                                FoodTitle = order1.FoodTitle,
                                Date = createInvoice,
                                WorkingDay = workingDay,
                                Number = "1",
                                Employee = await db.Users.FirstOrDefaultAsync(userEl => userEl.Id == model.User),
                                Crypt = false,
                                DepartmentId = order1.Department
                            };
                            db.Invoises.Add(invoice);
                        }
                        else
                        {
                            foodFromDB =
                                await
                                    db.Foods.Include(el => el.Category).Include(el => el.PriceWidths)
                                        .FirstOrDefaultAsync(el => el.Id == order1.FoodId);
                            //����� �� ��, ������� ��������� �� �� � ������� � ������
                            Invoice invoice = new Invoice
                            {
                                Food = foodFromDB, //��������� ����� �� �� ��������� � �� ������������� �����
                                Discount = order1.Discount,
                                //==true ? ((foodFromDB.PriceWidths.FirstOrDefault(priceEl=>priceEl.Actual && priceEl.Id==int.Parse//���� �� ��
                                //(order1.PriceWidthsKey)).Price * order1.Number) * foodFromDB.Category.Discount)/ 100 : 0,//�������� ���� �� ���������� � ������ ������/ �������� ������� �� �����

                                Table = await db.Tables.FirstOrDefaultAsync(el => el.Actual && el.Id == model.TableId),
                                Date = createInvoice,
                                PriceWidthId = int.Parse(order1.PriceWidthsKey),
                                WorkingDay = workingDay,
                                Number = Convert.ToString(order1.Number),
                                //crypto.GostEncryption("000000000000000"+Convert.ToString(order1.Number), _key),
                                Employee = await db.Users.FirstOrDefaultAsync(userEl => userEl.Id == model.User),
                                DepartmentId = order1.Department
                            };
                            db.Invoises.Add(invoice);
                        }

                    }
                    db.SaveChanges();
                }
            }

        }

        public String GetAndSaveImageOnServer(HttpPostedFileBase image, HttpServerUtilityBase server,
                                              String pathOnServer)
        {

            if(image == null) //& String.IsNullOrWhiteSpace( image.FileName ))//���� ����������� ���
                return "";

            var imageName = server.MapPath(pathOnServer) + image.FileName; //���������� ���� ����������� �� �������

            if(!Directory.Exists(server.MapPath(pathOnServer)))
            {
                //���� �����, ���� ����� ��������� ��������, ���, �� �������
                Directory.CreateDirectory(server.MapPath(pathOnServer));
            }

            image.SaveAs(imageName); //���������
            return pathOnServer.Replace("~", "") + Path.GetFileName(imageName);

        }

        /// <summary>
        /// ������������� ����������� ��� ������ � ��������� "���������"
        /// </summary>
        public class CategoryHelper
        {
            private const String categoryImageInProject = "~/Images/Category/";

            /// <summary>
            /// ���������� ���������� ���� �� �����������, ���� ��� ����, � ��������� ��� �� �����
            /// </summary>
            /// <param name="category">���������, � ������� ������� �����������</param>
            /// <param name="server"></param>
            /// <returns>��� ����������� � �������</returns>
            public String GetAndSaveCategoryImage(Category category, HttpServerUtilityBase server)
            {
                if(category.ImageFile.First() == null) //���� ����������� ���
                    return "";

                var imageName = server.MapPath(categoryImageInProject);
                imageName += category.ImageFile.First().FileName;

                if(!Directory.Exists(server.MapPath(categoryImageInProject)))
                {
                    //���� �����, ���� ����� ��������� ��������, ���, �� �������
                    Directory.CreateDirectory(server.MapPath(categoryImageInProject));
                }

                category.ImageFile.First().SaveAs(imageName); //���������
                return categoryImageInProject + Path.GetFileName(imageName);

            }

            /// <summary>
            /// ��������� ����� ��������� � ���� ������
            /// </summary>
            /// <param name="category">�������� - ���������</param>
            /// <returns>���������� ��� ���������� ������. -1(�� �������� ���������). 0 �� ������</returns>
            public async void AddCategory(Category category, HttpServerUtilityBase server)
            {
                ModelCafeHelper helper = new ModelCafeHelper();

                var imageName = helper.GetAndSaveImageOnServer(category.ImageFile.First(), server,
                    categoryImageInProject); //GetAndSaveCategoryImage( category, server );

                using(var db = new ApplicationDbContext())
                {
                    db.Categories.Add( //��������� ����� ���������
                        new Category
                        {
                            Actual = true,
                            Visible = true,
                            Title = category.Title,
                            CategoryId = category.CategoryId,
                            ImagePath = imageName,
                            Description = category.Description,
                            Discount = category.Discount,
                            DiscountBool = category.DiscountBool

                        });

                    /*await*/
                    await db.SaveChangesAsync();
                }
            }

            /// <summary>
            /// ���������� ��������� �� � ��������������
            /// </summary>
            /// <param name="id">������������� ���������</param>
            /// <returns>���������</returns>
            public Category GetCategory_FromId(Int32 id)
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Categories.First(el => el.Id == id);
                }
            }

            /// <summary>
            /// ���������� ��� ���������
            /// </summary>
            /// <returns>���������</returns>
            public IEnumerable<Category> GetCategories()
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Categories;
                }
            }

            /// <summary>
            /// �������� ��������� � ���� ������
            /// </summary>
            /// <param name="category">����������� ������� �������� ����� ���������, ������ ��������� ��������������</param>
            /// <returns>��� ����������. 0 �� ������</returns>
            public Int32 EditCategory(Category category)
            {
                using(var db = new ApplicationDbContext())
                {
                    var modifyCategory = db.Categories.First(el => el.Id == category.Id);

                    modifyCategory.ImagePath = category.ImagePath;
                    modifyCategory.Title = category.Title;
                    modifyCategory.ParentCategory = category.ParentCategory;
                    modifyCategory.DiscountBool = category.DiscountBool;
                    modifyCategory.Actual = category.Actual;
                    modifyCategory.Visible = category.Visible;

                    db.Entry(modifyCategory).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return 0;
            }

            /// <summary>
            /// ������� ��������� �� ���� ������
            /// </summary>
            /// <param name="id">������������� ���������, ������� ���������� �������</param>
            /// <returns>��� ����������. 0 �� ������</returns>
            public Int32 RemoveCategory(Int32 id)
            {
                using(var db = new ApplicationDbContext())
                {
                    var removeCategory = db.Categories.First(el => el.Id == id);
                    db.Categories.Remove(removeCategory);
                    db.SaveChanges();
                }

                return -1;
            }
        }

        /// <summary>
        /// ������������� ������ ��� ������ � �������
        /// </summary>
        public class FoodHelper
        {
            public const String foodImageInProject = "~/Images/Category/";

            ///// <summary>
            ///// ���������� ���������� ���� �� �����������, ���� ��� ����, � ��������� ��� �� �����
            ///// </summary>
            ///// <param name="category">���������, � ������� ������� �����������</param>
            ///// <param name="server"></param>
            ///// <returns>��� ����������� � �������</returns>
            //public String GetAndSaveImageOnServer( String fileName, HttpServerUtilityBase server, String pathOnServer )
            //{

            //    if (String.IsNullOrWhiteSpace( fileName ))//���� ����������� ���
            //        return "";

            //    var imageName = server.MapPath(foodImageInProject) + fileName;//���������� ���� ����������� �� �������

            //    if (!Directory.Exists( server.MapPath( foodImageInProject ) ))
            //    {
            //        //���� �����, ���� ����� ��������� ��������, ���, �� �������
            //        Directory.CreateDirectory( server.MapPath( foodImageInProject ) );
            //    }

            //    File.Create(imageName);//food.ImageFile.First().SaveAs( imageName );//���������
            //    return foodImageInProject + Path.GetFileName( imageName );

            //}

            /// <summary>
            /// ��������� �����
            /// </summary>
            /// <param name="food">�����, ������� ����� ��������</param>
            /// <param name="server"></param>
            /// <returns></returns>
            public async Task<Int32> AddFoods(Food food, HttpServerUtilityBase server)
            {
                ModelCafeHelper helper = new ModelCafeHelper();
                food.ImagePath = helper.GetAndSaveImageOnServer(food.ImageFile.First(), server, foodImageInProject);
                    //��������� �����������

                List<PriceWidth> priceWidths = new List<PriceWidth>();

                using(var db = new ApplicationDbContext())
                {

                    foreach(var item in food.PriceWidthsKey.Values.Where(item => item != null))
                    {
                        priceWidths.Add(new PriceWidth
                        { //��������� ����, ������� ������� � �����, ������� ������� � ���� ������
                            Actual = true,
                            DataChange = DateTime.UtcNow,
                            Price = item.Price,
                            Width = item.Width,
                            WeightNameId = item.WeightNameId,
                            CurrencyNameId = item.CurrencyNameId
                        });

                    }

                    db.Foods.Add(new Food
                    {
                        Actual = true,
                        Visible = true,
                        PriceWidths = priceWidths,
                        CategoryId = food.CategoryId,
                        Description = food.Description,
                        Title = food.Title,
                        ImagePath = food.ImagePath
                    });

                    await db.SaveChangesAsync();
                    return 1;
                }


            }




            /// <summary>
            /// ���������� ����� �� ��� ��������������
            /// </summary>
            /// <param name="id">������������� �����</param>
            /// <returns>������ �����, �� ���� ������</returns>
            public Food GetFood_FromId(Int32 id)
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Foods.First(el => el.Id == id);
                }
            }

            /// <summary>
            /// ���������� ��� �����
            /// </summary>
            /// <returns>��������� ����</returns>
            public IEnumerable<Food> GetCategories()
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Foods;
                }
            }

            /// <summary>
            /// �������� ����� � ���� ������
            /// </summary>
            /// <param name="food">�����, �� ������� ����� ��������. ������������� ������ �������������� �����������</param>
            /// <returns>��� ����������. 0 �� ������</returns>
            public Int32 EditFood(Food food)
            {
                using(var db = new ApplicationDbContext())
                {
                    var modifyFood = db.Foods.First(el => el.Id == food.Id);

                    modifyFood.Title = food.Title;
                    modifyFood.ImagePath = food.ImagePath;
                    modifyFood.Category = food.Category;
                    modifyFood.PriceWidths = food.PriceWidths;
                    modifyFood.Actual = food.Actual;
                    modifyFood.Visible = food.Visible;

                    db.Entry(modifyFood).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return 0;
            }

            ///// <summary>
            ///// ������� ����� �� ���� ������
            ///// </summary>
            ///// <param name="id">������������� �����, ������� ���������� �������</param>
            ///// <returns>��� ����������. 0 �� ������</returns>
            //public Int32 RemoveCategory(Int32 id)
            //{
            //    using (var db = new ApplicationDbContext())
            //    {
            //        var removeFood = db.Foods.First(el => el.Id == id);
            //        db.Foods.Remove(removeFood);
            //        db.SaveChanges();
            //    }

            //    return -1;
            //}
        }

        /// <summary>
        /// ������������� ������ ��� ������ � �������
        /// </summary>
        public class PricsWidthsHelper
        {
            /// <summary>
            /// ��������� ���� � ���
            /// </summary>
            /// <param name="priceWidth">�������� � ����� � �����</param>
            /// <returns>������������� �������� � ���� ������</returns>
            public Int32 AddFoods(PriceWidth priceWidth)
            {
                using(var db = new ApplicationDbContext())
                {

                    db.PriceWidths.Add( //��������� ����� ���� � ���
                        new PriceWidth
                        {
                            Price = priceWidth.Price,
                            Width = priceWidth.Width,
                            DataChange = DateTime.Now
                        });
                    try
                    {
                        db.SaveChanges();
                    }
                    catch
                    {
                        return db.PriceWidths.Last().Id; //���������� �������� ����������� �������������
                    }

                }

                return 0;
            }

        }

        /// <summary>
        /// ������������� ������ ��� ������ � ������
        /// </summary>
        public class InvoiceHelper
        {
            /// <summary>
            /// ��������� ���
            /// </summary>
            /// <param name="invoice">����������� ���</param>
            /// <returns></returns>
            public Int32 AddInvoice(Invoice invoice)
            {
                using(var db = new ApplicationDbContext())
                {
                    db.Invoises.Add( //��������� ����� ���������
                        new Invoice
                        {
                            Table = invoice.Table,
                            PriceWidth = invoice.PriceWidth,
                            Food = invoice.Food,
                            //User = invoice.User,



                        });
                    try
                    {
                        db.SaveChanges();
                    }
                    catch
                    {
                        return -1;
                    }

                }

                return 0;
            }

            /// <summary>
            /// ���������� ��� ����
            /// </summary>
            /// <returns>��������� �����</returns>
            public IEnumerable<Invoice> GetInvoices()
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Invoises;
                }
            }

        }

        /// <summary>
        /// ������������� ������ ��� ������ �� �������
        /// </summary>
        public class TableHelper
        {
            /// <summary>
            /// ��������� ����
            /// </summary>
            /// <param name="table"></param>
            /// <returns></returns>
            public Int32 AddTable(Table table)
            {
                using(var db = new ApplicationDbContext())
                {
                    db.Tables.Add( //��������� ����� ����
                        new Table
                        {
                            Title = table.Title,
                            Discription = table.Discription,
                            Actual = true,
                            Visible = true
                        });
                    try
                    {
                        db.SaveChanges();
                    }
                    catch
                    {
                        return -1;
                    }

                }

                return 0;
            }

            /// <summary>
            /// ���������� ���� �� ��� ��������������
            /// </summary>
            /// <param name="id">������������� �����</param>
            /// <returns>������ �����, �� ���� ������</returns>
            public Table GetTable_FromId(Int32 id)
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Tables.First(el => el.Id == id);
                }
            }

            /// <summary>
            /// ���������� ��� �����
            /// </summary>
            /// <returns>��������� ����</returns>
            public IEnumerable<Table> GetTables()
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Tables;
                }
            }

            /// <summary>
            /// �������� ���� � ���� ������
            /// </summary>
            /// <param name="table">����, ������� ����� ��������. ������������� ������ �������������� �����������</param>
            /// <returns>��� ����������. 0 �� ������</returns>
            public Int32 EditTable(Table table)
            {
                using(var db = new ApplicationDbContext())
                {
                    var modifyTable = db.Tables.First(el => el.Id == table.Id);

                    modifyTable.Title = table.Title;
                    modifyTable.Discription = table.Discription;
                    modifyTable.Actual = table.Actual;
                    modifyTable.Visible = table.Visible;

                    db.Entry(modifyTable).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return 0;
            }

            /// <summary>
            /// ������� ���� �� ���� ������
            /// </summary>
            /// <param name="id">������������� �����, ������� ���������� �������</param>
            /// <returns>��� ����������. 0 �� ������</returns>
            public Int32 RemoveTable(Int32 id)
            {
                using(var db = new ApplicationDbContext())
                {
                    var removeTable = db.Tables.First(el => el.Id == id);
                    db.Tables.Remove(removeTable);
                    db.SaveChanges();
                }

                return -1;
            }
        }


        /// <summary>
        /// ������������� ������ ��� ������ � ��������
        /// </summary>
        public class CommentHelper
        {
            /// <summary>
            /// ��������� ����� � ���� ������
            /// </summary>
            /// <param name="coment">������ - �����</param>
            /// <returns></returns>
            public Int32 AddFoods(Comment coment)
            {
                using(var db = new ApplicationDbContext())
                {
                    db.Comments.Add( //��������� ����� �����
                        new Comment
                        {
                            CommentText = coment.CommentText,
                            Food = coment.Food,
                            Table = coment.Table

                        });
                    try
                    {
                        db.SaveChanges();
                    }
                    catch
                    {
                        return -1;
                    }

                }

                return 0;
            }

            /// <summary>
            /// ���������� ����� �� �������������� �����
            /// </summary>
            /// <param name="id">������������� �����</param>
            /// <returns>������ ������, �� ���� ������</returns>
            public Comment GetComment_FromId(Int32 id)
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Comments.FirstOrDefault(el => el.FoodId == id);
                }
            }

            /// <summary>
            /// ���������� ��� ������
            /// </summary>
            /// <returns>��������� �������</returns>
            public IEnumerable<Comment> GetComments()
            {
                using(var db = new ApplicationDbContext())
                {
                    return db.Comments;
                }
            }

        }

        public class UserHelper
        {
            public WorkDayStatus AddRandomKey(String UserID)
            {
                Random rnd = new Random();
                SetOfStatusMess mes = new SetOfStatusMess();
                using(var db = new ApplicationDbContext())
                {
                    ApplicationUser firstOrDefault = db.Users.FirstOrDefault(el => el.Id == UserID);

                    var random = Convert.ToString(rnd.Next(0, 1000000)); //������� ���� ��� ������ �����������
                    firstOrDefault.RandomKey = random;
                    db.Entry(firstOrDefault).State = EntityState.Modified;
                    db.SaveChanges(); //��������� �������� � �� �������� ����� ��� �����

                    return new WorkDayStatus
                    {
                        Status = mes.identifiKeySuccess,
                        Key = random
                    };
                }
            }


        }
    }

}