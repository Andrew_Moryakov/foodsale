﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ProjectAmur.Models;

namespace ProjectAmur.Controllers.Helpers
{
    class OtherHelpers
    {

        /// <summary>
        /// Возвращает Список список блюд у каждого элемента есть поле количество.
        /// </summary>
        /// <returns>Словарь</returns>
        public Dictionary<String, NumberFood> GetKeysFoods()
        {
            using (var db = new ApplicationDbContext())
            {
                //Хранит список блюд и ключ, который идентифицирует категории, так как на странице одинаковых категори може быть много
                List<Food> foods = db.Foods.Include(el => el.PriceWidths).Include(el => el.Category).Include(el => el.Category.ParentCategory).ToList();
                //Получаем все блюда. Включаем категорию и цены блюда

                return foods.ToDictionary(item => Guid.NewGuid().ToString("N"), item => new NumberFood(item));
            }
        }

        public List<String> RecoveryLostOrders()
        {
            HttpCookie aCookie = new HttpCookie("userInfo");
          

            return new List<string>();
        }
       
    }
}
