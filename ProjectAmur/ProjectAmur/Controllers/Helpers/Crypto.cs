﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAmur.Controllers.Helpers
{
    public class Crypto
    {
        public byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length];
            int i = 0;
            foreach (char ch in str)
            {

                    bytes[i] = Convert.ToByte(ch);
                i++;
            }
            return bytes;
        }

        public string GetString(byte[] bytes)
        {
            var result = new char[bytes.Length];
            int i = 0;
            foreach (byte item in bytes)
            {
                result[i] = Convert.ToChar(item);
                i++;
            }
            return new string(result);
        }

        public static byte[,] P
        {
            get { return p; }
        }

        //Таблицы замены из RFC4357
        private static readonly byte[,] p =
        {
            {0x04,0x0a,0x09,0x02,0x0d,0x08,0x00,0x0e,0x06,0x0B,0x01,0x0c,0x07,0x0f,0x05,0x03},
            {0x0e,0x0b,0x04,0x0c,0x06,0x0d,0x0f,0x0a,0x02,0x03,0x08,0x01,0x00,0x07,0x05,0x09},
            {0x05,0x08,0x01,0x0d,0x0a,0x03,0x04,0x02,0x0e,0x0f,0x0c,0x07,0x06,0x00,0x09,0x0b},
            {0x07,0x0d,0x0a,0x01,0x00,0x08,0x09,0x0f,0x0e,0x04,0x06,0x0c,0x0b,0x02,0x05,0x03},
            {0x06,0x0c,0x07,0x01,0x05,0x0f,0x0d,0x08,0x04,0x0a,0x09,0x0e,0x00,0x03,0x0b,0x02},
            {0x04,0x0b,0x0a,0x00,0x07,0x02,0x01,0x0d,0x03,0x06,0x08,0x05,0x09,0x0c,0x0f,0x0e},
            {0x0d,0x0b,0x04,0x01,0x03,0x0f,0x05,0x09,0x00,0x0a,0x0e,0x07,0x06,0x08,0x02,0x0c},
            {0x01,0x0f,0x0d,0x00,0x05,0x07,0x0a,0x04,0x09,0x02,0x03,0x0e,0x06,0x0b,0x08,0x0c}
        };


        private byte[] Alignment(string plainText, int block)
        {
            #region Делаем тектс кратным восьми. В конец добавляем 1, 0 и число которое говорит сколько мы добавили нулей, чтобы текст стал кратным восьми
            byte different = 0;
            var firstLength = plainText.Length;
            List<byte> tempOt = new List<byte>(GetBytes(plainText));
            if (((plainText.Length % block) != 0))//Если не кратен
            {
                while (((tempOt.Count % block) != 0))
                {
                    tempOt.Add(0); //Дополняем нулями, до кратности
                }
            }
            else
            {//Если кратен, то добавляем 8 нулей, чтобы мы точно были уверены, что после дешифрации мы не обрежем исходный текст, если предпоследним будет ноль. А так мы точно знаем 7 нулей(на 8 ноль поставим количество для обрезани) значит обрезаем.
                for (int i = 0; i < block-1; i++)
                {
                    tempOt.Add(0);
                }
            }

            different = Convert.ToByte(tempOt.Count - firstLength);
            if (different > 0)
            {
                tempOt.RemoveRange(tempOt.Count - 1, 1);//Удаляем последний ноль
                tempOt.Add(different);//Заменяем его битом, который говорит, какой длины дополнение
            }

            return tempOt.ToArray();

            #endregion
        }
        public string GostEncryption(string plainText, string key)
        {
            var k = Alignment(key, 32);
            byte[] ot = Alignment(plainText, 8);
            byte[] st = new byte[ot.Length];

            if (ot.Length != st.Length) throw new ArgumentException("Invalid input arrays length");
            if ((ot.Length % 8) != 0) throw new ArgumentException("Invalid input arrays length");
            int offset = 0;
            while (offset < ot.Length)
            {
                uint tempOutH = 0;
                uint tempOutL = 0;
                uint tempInH = Bytes2Dword(ot, offset);
                uint tempInL = Bytes2Dword(ot, offset + 4);
                EncryptBlock(ref tempInH, ref tempInL, ref tempOutH, ref tempOutL, p, k);
                Array.Copy(Dword2Bytes(tempOutH), 0, st, offset, 4);
                Array.Copy(Dword2Bytes(tempOutL), 0, st, offset + 4, 4);
                offset += 8;
            }
          
            return GetString(st);
        }

        public string GostDecryption(string plainText, string key)
        {
            var ot = GetBytes(plainText);
            var k = Alignment(key, 32);
            byte[] st = ot;


            if (ot.Length != st.Length) throw new ArgumentException("Invalid input arrays length");

            if ((st.Length % 8) != 0) throw new ArgumentException("Invalid input arrays length");
            int offset = 0;
            while (offset < ot.Length)
            {
                uint tempOutH = 0;
                uint tempOutL = 0;
                uint tempInH = Bytes2Dword(st, offset);
                uint tempInL = Bytes2Dword(st, offset + 4);
                DecryptBlock(ref tempInH, ref tempInL, ref tempOutH, ref tempOutL, p, k);
                Array.Copy(Dword2Bytes(tempOutH), 0, ot, offset, 4);
                Array.Copy(Dword2Bytes(tempOutL), 0, ot, offset + 4, 4);
                offset += 8;
            }

            if(st[st.Length - 2] == 0)
            {
                List<byte> tempST = new List<byte>(st);

                tempST.RemoveRange(st.Length - st[st.Length - 1], st[st.Length - 1]);
                st = tempST.ToArray();
            }
            var reslt = GetString(st);
            return reslt;
        }


        #region Bytes2DWORD

        private byte[] Dword2Bytes(uint input)
        {
            return new[]
            {
                (byte)(input & 0x000000ff),
                (byte)((input>>8)&0x000000ff),
                (byte)((input>>16)&0x000000ff),
                (byte)((input >> 24) & 0x000000ff)
            };
        }

        private uint Bytes2Dword(byte[] input, int offset)
        {
            if (offset + 4 > input.Length) throw new ArgumentOutOfRangeException();
            return (uint)((input[offset + 3] << 24) ^ (input[offset + 2] << 16) ^ (input[offset + 1] << 8) ^ (input[offset]));
        }

        #endregion

        #region Additional Functions

        private void DecryptBlock(ref uint inH, ref uint inL, ref uint outH, ref uint outL, byte[,] pi, byte[] K)
        {
            uint m1 = inH;
            uint m2 = inL;
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 0));   //K0
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 4));   //K1
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 8));   //K2
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 12));  //K3
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 16));  //K4
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 20));  //K5
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 24));  //K6
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 28));  //K7

            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 28));  //K7
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 24));  //K6
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 20));  //K5
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 16));  //K4
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 12));  //K3
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 8));   //K2
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 4));   //K1
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 0));   //K0

            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 28));  //K7
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 24));  //K6
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 20));  //K5
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 16));  //K4
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 12));  //K3
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 8));   //K2
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 4));   //K1
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 0));   //K0

            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 28));  //K7
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 24));  //K6
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 20));  //K5
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 16));  //K4
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 12));  //K3
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 8));   //K2
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 4));   //K1

            outH = m1;
            outL = Pi(pi, m1 + Bytes2Dword(K, 0));
            outL = (outL << 11) | (outL >> 21);
            outL ^= m2;
        }

        private void EncryptBlock(ref uint inH, ref uint inL, ref uint outH, ref uint outL, byte[,] pi, byte[] K)
        {
            uint m1 = inH;
            uint m2 = inL;
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 0));   //K0
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 4));   //K1
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 8));   //K2
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 12));  //K3
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 16));  //K4
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 20));  //K5
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 24));  //K6
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 28));  //K7

            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 0));   //K0
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 4));   //K1
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 8));   //K2
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 12));  //K3
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 16));  //K4
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 20));  //K5
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 24));  //K6
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 28));  //K7

            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 0));   //K0
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 4));   //K1
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 8));   //K2
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 12));  //K3
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 16));  //K4
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 20));  //K5
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 24));  //K6
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 28));  //K7

            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 28));  //K7
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 24));  //K6
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 20));  //K5
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 16));  //K4
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 12));  //K3
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 8));   //K2
            GostTransform(ref m1, ref m2, pi, Bytes2Dword(K, 4));   //K1

            outH = m1;
            outL = Pi(pi, m1 + Bytes2Dword(K, 0));
            outL = (outL << 11) | (outL >> 21);
            outL ^= m2;
        }



        private void GostTransform(ref uint inBlockH, ref uint inBlockL, byte[,] pi, uint k)
        {
            uint outBlockH = Pi(pi, inBlockH + k);
            outBlockH = (outBlockH << 11) | (outBlockH >> 21);
            outBlockH ^= inBlockL;
            uint outBlockL = inBlockH;
            //копируем все на выход
            inBlockL = outBlockL;
            inBlockH = outBlockH;
        }

        private uint Pi(byte[,] sBox, uint inBlock)
        {
            if (sBox == null) throw new ArgumentNullException("sBox");
            uint res = 0;
            res ^= sBox[0, (inBlock & 0x0000000f)];
            res ^= (uint)(sBox[1, ((inBlock & 0x000000f0) >> 4)] << 4);
            res ^= (uint)(sBox[2, ((inBlock & 0x00000f00) >> 8)] << 8);
            res ^= (uint)(sBox[3, ((inBlock & 0x0000f000) >> 12)] << 12);
            res ^= (uint)(sBox[4, ((inBlock & 0x000f0000) >> 16)] << 16);
            res ^= (uint)(sBox[5, ((inBlock & 0x00f00000) >> 20)] << 20);
            res ^= (uint)(sBox[6, ((inBlock & 0x0f000000) >> 24)] << 24);
            res ^= (uint)(sBox[7, ((inBlock & 0xf0000000) >> 28)] << 28);
            return res;
        }
        #endregion
    }
}
