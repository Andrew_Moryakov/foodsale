﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectAmur.Controllers.Helpers;
using ProjectAmur.Models;
using WebGrease.Css.Extensions;

namespace ProjectAmur.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminApiController : ApiController
    {
        public HttpResponseMessage ToJson(String json)
        {
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }

        class RatingFood
        {
            public String FoodTitle { get; set; }
            public Int32 Number { get; set; }
            public Double Sum { get; set; }
        }


        [HttpGet]
        public HttpResponseMessage GetWorkDays()
        {
            using (var db = new ApplicationDbContext())
            {
                var days = db.WorkingDay.Include(el => el.Employee).ToList();
                return ToJson(JsonConvert.SerializeObject(days.Select(el => new
                {
                    OpenDay = el.OpeningDay,
                    CloseDay = el.ClosingDay,
                    NameUser = db.Users.FirstOrDefault(user => user.Id == el.UserId).Name,
                    SurNameUser = db.Users.FirstOrDefault(user => user.Id == el.UserId).SurName
                })));
            }
        }

        public class CategoryForDrop
        {
            public String CategoryTitle { get; set; }
            public Int32 CategoryID { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage GetForDropCategories()
        { 
            List<CategoryForDrop> Categories = new List<CategoryForDrop>();
            using (var db = new ApplicationDbContext())
            {
                foreach(var item in db.Categories.Where(el=>el.Actual && el.Visible))
                {
                  Categories.Add(new CategoryForDrop
                  {
                      CategoryID = item.Id,
                      CategoryTitle=item.Title
                  });
                }
            }
            return ToJson(JsonConvert.SerializeObject(Categories));

        }
        public class CategoryIncome
        {
            [UIHint("FileUpload")]
            public string ImageUrl { get; set; }

            public string FileName { get; set; }
            public String CategoryTitle {get; set; }
            public CategoryForDrop ParentCategory { get; set; }
            public double  Discount { get; set; }
            public String ImagePath { get; set; }
            public Double? Income { get; set; }
            public Int32 CategoryID { get; set; }
        }


        [HttpGet]
        public void UpdateCategories(String models)
        {
            var r = JsonConvert.DeserializeObject<CategoryIncome[]>(models); 
        }
        [HttpGet]
        public void DestroyCategories()
        {
        }
        [HttpGet]
        public void CreateCategories()
        {
        }

        [HttpGet]
        public List<CategoryIncome> GetCategories()
        { 
            List<CategoryIncome> CatigoriesIncome = new List<CategoryIncome>();
            using (var db = new ApplicationDbContext())
            {

                var categories = db.Categories.Include(c => c.ParentCategory).Where(el => el.Actual && el.Visible);//подсоединяем подкатегории
                                                                                                                 
                foreach (var category in categories)
                {
                    double? sum = 0;
                    var category1 = category;
                    foreach (var food in db.Foods.Include(el => el.Category).Include(el => el.PriceWidths).Where(food => food.Actual && food.Visible && food.CategoryId == category1.Id))
                    {
                        var s = db.Invoises.ToList().Where(inv => inv != null && inv.FoodId != null && inv.FoodId == food.Id)?.Where(foodEl => foodEl.PriceWidth != null && foodEl.PriceWidth.Price != null && foodEl.PriceWidth.Actual)?.Select(inv => inv.PriceWidth.Price).Sum();
                        if (s != null)
                        {
                            sum += (double)s;
                        }

                    }
                    CatigoriesIncome.Add(new CategoryIncome
                    {
                        CategoryID = category1.Id,
                        CategoryTitle = category1.Title,
                        Discount = category1.Discount,
                        ImagePath = category1.ImagePath ?? "",
                        Income = sum,
                        ParentCategory =   new CategoryForDrop {
                        CategoryTitle = category1.ParentCategory==null || category1.ParentCategory.Title==null?"":category1?.ParentCategory?.Title,
                        CategoryID =  category1.ParentCategory?.Id ?? -1
                        }
                    });
                }
            }


            return CatigoriesIncome; // ToJson(JsonConvert.SerializeObject(CatigoriesIncome));
        }


        [HttpGet]
        public void UpdateFoods(String models)
        {
            var r = JsonConvert.DeserializeObject<CategoryIncome[]>(models);
        }
        [HttpGet]
        public void DestroyFoods()
        {
        }
        [HttpGet]
        public void CreateFoods()
        {
        }

        [HttpGet]
        public HttpResponseMessage GetFoods()
        {
            using(var db = new ApplicationDbContext())
            {

                var foods =db.Foods.Include(f => f.Category).Include(elPrice => elPrice.PriceWidths).Where(food => food.Actual && food.Visible);
                foods.ForEach( food => food.PriceWidths.ForEach( price => price.WeightName = db.WeightNames.FirstOrDefault(wn => wn.Id == price.WeightNameId)));
                foods.ForEach(food =>food.PriceWidths.ForEach( price =>price.CurrencyName = db.CurrencyNames.FirstOrDefault(сn => сn.Id == price.CurrencyNameId)));
                foreach(var item in foods)
                {
                    item.PriceWidths = item.PriceWidths.Where(el => el.Actual).ToList();
                }
                return ToJson(JsonConvert.SerializeObject(foods));
            }
        
        }

        [HttpGet]
        public HttpResponseMessage GetCurrency()
        {
            using (var db = new ApplicationDbContext())
            {
                return ToJson(JsonConvert.SerializeObject(db.CurrencyNames.ToList()));
            }

        }

        [HttpGet]
        public HttpResponseMessage GetUsers()
        {
            using (var db = new ApplicationDbContext())
            {
                return ToJson(JsonConvert.SerializeObject(db.Users.Where(el => el.Actual).ToList()));
            }

        }

        [HttpGet]
        public HttpResponseMessage GetDepartments()
        {
            using (var db = new ApplicationDbContext())
            {
                return ToJson(JsonConvert.SerializeObject(db.Departments.Where(el => el.Actual && el.Visible).ToList()));
            }

        }

        [HttpGet]
        public HttpResponseMessage GetTables()
        {
            using (var db = new ApplicationDbContext())
            {
                return ToJson(JsonConvert.SerializeObject(db.Tables.Where(el => el.Actual && el.Visible).ToList()));
            }

        }

        [HttpGet]
        public HttpResponseMessage GetWidths()
        {
            using (var db = new ApplicationDbContext())
            {
                return ToJson(JsonConvert.SerializeObject(db.WeightNames.ToList()));
            }

        }
        [HttpGet]
        public HttpResponseMessage GetRatingFood()
        {
            using (var db = new ApplicationDbContext())
            {

                //return PartialView("~/Views/PartialView/_RatingFoods.cshtml", new List<Invoice>((
                Dictionary<int?, List<Invoice>> rating = db.Invoises
                    .Include("Food")
                    .Include("PriceWidth")
                    .Include("Table")
                    .Include("WorkingDay")//Подключаем связанные таблицы
                    .ToList<Invoice>().Where(el=>el.FoodId!=null)
                    .GroupBy(el => el?.FoodId)//Груперуем по блюдам (ключ блюдо)
                    .ToDictionary(el => el.Key, el => el.ToList())//Превращаем в словарь
                    .OrderByDescending(el => el.Value.Count)//Сортируем словарь по количеству в нем элементов. То есть сверху самые часто продаваемые блюда
                    .ToDictionary(el => el.Key, el => el.Value);//Опять в словарь

        
                var reatingList = new List<RatingFood>(); 
                    Crypto crypto = new Crypto();
                    foreach (KeyValuePair<int?, List<Invoice>> itemR in rating)
                    {
                        reatingList.Add(new RatingFood
                        {
                            Sum = itemR.Value.Select(el => Double.Parse(crypto.GostDecryption(el.Number, "Password123456")) * el.PriceWidth.Price - (el.Discount ? (((Double.Parse(crypto.GostDecryption(el.Number, "Password123456")) * el.PriceWidth.Price) * db.Categories.FirstOrDefault(categ => categ.Id == el.Food.CategoryId).Discount) / 100) : 0)).Sum(el => el),
                            Number = itemR.Value.Select(el=> int.Parse(crypto.GostDecryption(el.Number, "Password123456"))).Sum(el=>el),
                            FoodTitle = itemR.Value.FirstOrDefault().Food.Title
                        });
                        //Добавляем в коллекцию (в скольких чеках было то или иное блюдо)
                    } //Таким образом показывает рейтинг по частоте продаж блюд в разных чеках.

                return ToJson(JsonConvert.SerializeObject(reatingList));
            }
        }

        [HttpGet]
        public HttpResponseMessage GetInvoices()
        {
            using (var db = new ApplicationDbContext())
            {
                Crypto crypto = new Crypto();

                foreach(var item in db.Invoises.Where(el => el.Crypt == false).ToList())
                {
                    item.Number= crypto.GostEncryption(item.Number, "Password123456");
                    item.Crypt = true;
                    db.Entry(item).State = EntityState.Modified;
                }
                db.SaveChanges();


                //var reslt =
                //    db.Invoises.Include("Food")
                //    .Include("PriceWidth")
                //    .Include("Table")
                //    .Include("WorkingDay")
                //    .Include("Employee")
                //    .Include("Department").ToList();

                //List<object> listObj = new List<object>();
                //foreach (var el in reslt)
                //{
                //    var FoodTitle = el.Food == null ? "" : el?.Food.Title;
                //    var Sum = (Double.Parse(crypto.GostDecryption(el.Number, "Password123456"))
                //        * (el.PriceWidth == null ? el.Price : el.PriceWidth.Price))
                //        - (el.Discount
                //            ? (((Double.Parse(crypto.GostDecryption(el.Number, "Password123456")) * el.PriceWidth.Price)
                //                * db.Categories.FirstOrDefault(categ => categ.Id == el.Food.CategoryId).Discount) / 100)
                //            : 0);
                //    var SumDiscount = el.Discount
                //        ? ((Double.Parse(crypto.GostDecryption(el.Number, "Password123456"))
                //            * (el.PriceWidth == null ? el.Price : el.PriceWidth.Price))
                //            * db.Categories.FirstOrDefault(categ => categ.Id == el.Food.CategoryId).Discount) / 100 : 0;
                //    var Number = Double.Parse(crypto.GostDecryption(el.Number, "Password123456"));
                //        var PercentageDiscount = el.Discount
                //        ? db.Categories.FirstOrDefault(categ => categ.Id == el.Food.CategoryId).Discount : 0;
                //    var NameUser = db.Users.FirstOrDefault(user => user.Actual && user.Id == el.UserId).Name;
                //    var SurNameUser = db.Users.FirstOrDefault(user => user.Actual && user.Id == el.UserId).SurName;
                //    var DateOrder = el.Date;
                //    var TableOrder = el.Table == null ? "" : el.Table.Title;


                //}

                return ToJson(JsonConvert.SerializeObject(db.Invoises
                    .Include("Food")
                    .Include("PriceWidth")
                    .Include("Table")
                    .Include("WorkingDay")
                    .Include("Employee")
                    .Include("Department").ToList().Select(el => new
                    {
                        FoodTitle = el.FoodTitle ?? el?.Food?.Title,
                        Sum = (Double.Parse(crypto.GostDecryption(el.Number, "Password123456")) * (el.PriceWidth==null?el.Price: el.PriceWidth.Price)) - (el.Discount?(((Double.Parse(crypto.GostDecryption(el.Number, "Password123456")) * el.PriceWidth.Price) * db.Categories.FirstOrDefault(categ=>categ.Id == el.Food.CategoryId).Discount) / 100):0),
                        SumDiscount = el.Discount?((Double.Parse(crypto.GostDecryption(el.Number, "Password123456")) * (el.PriceWidth == null ? el.Price : el.PriceWidth.Price)) * db.Categories.FirstOrDefault(categ => categ.Id == el.Food.CategoryId).Discount)/100 : 0,
                        Number = Double.Parse(crypto.GostDecryption(el.Number, "Password123456")), 
                        PercentageDiscount = el.Discount?db.Categories.FirstOrDefault(categ => categ.Id == el.Food.CategoryId).Discount:0,
                        NameUser = db.Users.FirstOrDefault(user => user.Actual && user.Id == el.UserId).Name,
                        SurNameUser = db.Users.FirstOrDefault(user => user.Actual && user.Id == el.UserId).SurName,
                        DateOrder = el.Date,
                        TableOrder = el.Table==null?"":el.Table.Title,
                        Department = el.Department == null ? "" : el.Department.Title,
                    })));

            }
        }




    }
}
